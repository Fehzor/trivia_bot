/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bot.Commands.Implementation;

import Bot.Commands.Command;
import Bot.Commands.CommandParser;
import Bot.Fields.UserData;
import Bot.Launcher;
import static Bot.SuperRandom.oRan;
import Main.Channel.LocationChannel;
import Main.Structure.Item;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author FF6EB4
 */
public class Consume extends Command{
    
    public Consume(){
        this.category = 1;
        this.signature = new String[]{"consume","eat","drink"};
        this.description = "consumes something";
    }
    
    public void execute(String params, long ID){
        try{
        int num = 1;
        if(params.contains("all")){
            params=params.replace("all ", "");
            num = Integer.MAX_VALUE;
        }
        
        
        Item I = Item.getItem(WordUtils.capitalizeFully(params));
        
        UserData UD = UserData.getUD(ID);
        
        while(num>0&&UD.inventory.getData().containsKey(I.ID)&& UD.inventory.getData().get(I.ID)>0){
            num--;
            I.consume(LocationChannel.getChannel(UD.lastChannel), UD);
            UD.giveItem(I, -1);
        }
        
        } catch (Exception E){
            E.printStackTrace();
        }
    }
}