/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bot.Commands.Implementation;

import Bot.Commands.Command;
import Bot.Commands.CommandParser;
import Bot.Fields.UserData;
import Bot.Launcher;
import static Bot.SuperRandom.oRan;
import Main.Channel.LocationChannel;
import Main.Structure.Item;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author FF6EB4
 */
public class Equip extends Command{
    
    public Equip(){
        this.category = 1;
        this.signature = new String[]{"equip","eq"};
        this.description = "equips something";
    }
    
    public void execute(String params, long ID){
        try{
        Item I = Item.getItem(WordUtils.capitalizeFully(params));
        
        UserData UD = UserData.getUD(ID);
        
        if(UD.inventory.getData().containsKey(I.ID)&& UD.inventory.getData().get(I.ID)>0){
            UD.giveItem(Item.getItem(UD.weapon.getData()), 1);
            UD.weapon.writeData(I.ID);
            UD.giveItem(I, -1);
            
            Launcher.send("Equipped "+I.name.getData());
        }
        
        } catch (Exception E){
            E.printStackTrace();
        }
        
    }
}