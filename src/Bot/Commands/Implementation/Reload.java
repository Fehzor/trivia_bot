/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bot.Commands.Implementation;

import Bot.Commands.Command;
import Bot.Commands.CommandParser;
import Bot.Fields.UserData;
import Bot.Launcher;
import static Bot.SuperRandom.oRan;
import Games.EmojiDex;
import Main.Structure.Feature;
import Main.Channel.LocationChannel;
import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

/**
 *
 * @author FF6EB4
 */
public class Reload extends Command{
    
    public Reload(){
        this.category = 1;
        this.signature = new String[]{"!reload"};
        this.description = "Reloads the entire world- Dreamer only";
    }
    
    public void execute(String params, long ID){
        try{
            UserData UD = UserData.getUD(ID);
            IUser IU = Launcher.client.fetchUser(ID);
            IRole dreamer = Launcher.client.getRoleByID(471445546570219521L);
            if(IU.hasRole(dreamer)){
                
                LocationChannel.deleteAll();
                LocationChannel.loadFromFile();
                
                for(String IDs: UserData.IDList.getData()){
                    UserData ud = UserData.getUD(Long.parseLong(IDs));
                    ud.rerole();
                }

                System.out.println("IT HAS BEEN DONE");
            } else {
                System.out.println("FAILED TO FIND DREAMER ROLE");
            }
        } catch (Exception E){E.printStackTrace();}
    }
}