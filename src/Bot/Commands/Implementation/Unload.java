/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bot.Commands.Implementation;

import Bot.Commands.Command;
import Bot.Commands.CommandParser;
import Bot.Fields.UserData;
import Bot.Launcher;
import static Bot.SuperRandom.oRan;
import Games.EmojiDex;
import Main.Structure.Feature;
import Main.Channel.LocationChannel;
import Main.Structure.Item;
import Main.Structure.StateMachine;
import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

/**
 *
 * @author FF6EB4
 */
public class Unload extends Command{
    
    public Unload(){
        this.category = 1;
        this.signature = new String[]{"!unload"};
        this.description = "Unloads the entire world- Dreamer only";
    }
    
    public void execute(String params, long ID){
        try{
            UserData UD = UserData.getUD(ID);
            IUser IU = Launcher.client.fetchUser(ID);
            IRole dreamer = Launcher.client.getRoleByID(471445546570219521L);
            if(IU.hasRole(dreamer)){
                
                LocationChannel.deleteAll();
                Feature.deleteAll();
                Item.deleteAll();
                StateMachine.deleteAll();

                System.out.println("IT HAS BEEN DONE");
            } else {
                System.out.println("FAILED TO FIND DREAMER ROLE");
            }
        } catch (Exception E){E.printStackTrace();}
    }
}