package Bot.Fields;

import Bot.Fields.Field;
import Bot.Launcher;
import static Bot.SuperRandom.oRan;
import Games.EmojiDex;
import Main.Channel.LocationChannel;
import Main.Structure.Item;
import Main.Structure.Stats;
import static Main.Structure.Stats.EMPATHY;
import static Main.Structure.Stats.ENDURANCE;
import static Main.Structure.Stats.LIBIDO;
import static Main.Structure.Stats.LIFE_FORCE;
import static Main.Structure.Stats.LUCK;
import static Main.Structure.Stats.PHYSICAL;
import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import java.awt.Color;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.RoleBuilder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author FF6EB4
 */
public class UserData {
    public static Field<ArrayList<String>> IDList = new Field<>("USERDATA","IDLIST",new ArrayList<>());
    private static HashMap<String,UserData> UserList = new HashMap<>();
    
    public static UserData getUD(IUser user){
        if(!UserList.containsKey(user.getStringID())){
            if(!IDList.getData().contains(user.getStringID())){
                IDList.getData().add(user.getStringID());
            }
            UserList.put(user.getStringID(),new UserData(user));
        }
        
        IDList.writeData(IDList.getData());
        
        //if(UserList.get(user.getID()).name.equals("Clint Eastwood's Character")){
        //    UserList.get(user.getID()).name = user.getName();
        //}
        
        return UserList.get(user.getStringID());
    }
    
    public static UserData getUD(long ID){
        return getUD(Launcher.client.getUserByID(ID));
    }
    
    
    public String name = "Nameless Hero Of Legend";
    public String ID = "00002";
    
    public Field<Integer> lols;
    public Field<HashMap<String,Integer>> emoji;
    public Field<HashMap<String,Integer>> fish;
    public Field<Integer> blocks;
    
    //public Field<Integer> solutions;
    //public Field<Integer> dots;
    
    public Field<ArrayList<String>> areas;
    
    public Field<Long> LifeForceEXP;
    public Field<Long> LifeForce;
    
    public Field<Long> PhysEXP;
    public Field<Long> EndureEXP;
    public Field<Long> EmpEXP;
    public Field<Long> LuckEXP;
    public Field<Long> LibidoEXP;
    
    public Field<HashMap<Long,Long>> inventory;
    public Field<Long> weapon;
    
    public Field<Long> role;
    
    public Field<Long> lastMessage;
    public IChannel lastChannel;
    
    public UserData(IUser user){
        instantiateFields(user);
    }
    
    public String toString(){
        String s = "**"+name+"**\n\n";
        s+="**RESOURCES:**\n";
        s+="**Lols Received:** "+lols.getData()+"\n";
        s+="**Messages:** "+blocks.getData()+"\n";
        //s+="**Trivia Points:** "+solutions.getData()+"\n";
        //s+="**Orbs:** "+dots.getData()+"\n";
        
        String emojis = "";
        for(String key : fish.getData().keySet()){
            if(fish.getData().get(key) > 0){
                emojis+=key+"x"+fish.getData().get(key)+" ";
            }
        }
        s+="**Emoji:** "+emojis+"\n";
        
        String items = "";
        
        for(Long key : inventory.getData().keySet()){
            if(inventory.getData().get(key) > 0){
                Item I = Item.getItem(key);
                items+=I.name.getData()+" x"+inventory.getData().get(key)+"\n";
            }
        }
        
        s+="\n**ITEMS:**\n"+items+"\n";
        
        s+="\n**EQUIPPED: **"+Item.getItem(weapon.getData()).name.getData();
        
        return s;
    }
    
    public String toStats(){
        String s = "**"+name+"**\n\n";
        s+="**STATS:**\n";
        s+="**Life Force:** "+LifeForce.getData()+"/"+Stats.getLevel(this,LIFE_FORCE)+"\n";
        s+="**Physical:** "+Stats.getLevel(this,PHYSICAL)+"\n";
        s+="**Endurance:** "+Stats.getLevel(this,ENDURANCE)+"\n";
        s+="**Empathy:** "+Stats.getLevel(this,EMPATHY)+"\n";
        s+="**Luck:** "+Stats.getLevel(this,LUCK)+"\n";
        s+="**Libido:** "+Stats.getLevel(this,LIBIDO)+"\n";
        
        
        return s;
    }
    
    private void instantiateFields(IUser user){
        
        this.name = user.getName();
        this.ID = user.getStringID();
        
        lols = new Field<>(this.ID,"lols",0);
        emoji = new Field<>(this.ID,"emoji",new HashMap<>());
        fish = new Field<>(this.ID,"fish",new HashMap<>());
        blocks = new Field<>(this.ID,"blocks",0);
        
        //solutions = new Field<>(this.ID,"solutions",0);
        //dots = new Field<>(this.ID,"dots",0);
        
        LifeForceEXP = new Field<>(this.ID,"LFEXP",0L);
        LifeForce = new Field<>(this.ID,"LF",1L);
        
        PhysEXP = new Field<>(this.ID,"PHEXP",0L);
        EndureEXP = new Field<>(this.ID,"ENDXP",0L);
        EmpEXP = new Field<>(this.ID,"EMPEXP",0L);
        LuckEXP = new Field<>(this.ID,"LUKEXP",0L);
        LibidoEXP = new Field<>(this.ID,"LIBEXP",0L);
        
        inventory = new Field<>(this.ID,"INVENTORY",new HashMap<>());
        
        try{
        Item starting = Item.getItem("Unarmed");
        weapon = new Field<>(this.ID,"WEAPON",starting.ID);
        } catch (Exception E){
            Item.deleteAll();
            Item.loadFromFile();
            Item starting = Item.getItem("Unarmed");
            weapon = new Field<>(this.ID,"WEAPON",starting.ID);
        }
        
        role = new Field<>(this.ID,"role",this.getRoleIfApplicable(ID).getLongID());
        
        IRole theRole = Launcher.client.getRoleByID(role.getData());
        user.addRole(theRole);
        
        this.lastMessage = new Field<>(this.ID,"lastMessage",0L);
        
        this.areas = new Field<>(this.ID,"AREAS",new ArrayList<>());
        
        rerole();
    }
    
    public void rerole(){
        for(String s : areas.getData()){
            IUser ourUser = Launcher.client.getUserByID(Long.parseLong(ID));
            try{
                for(IChannel get : Launcher.client.getChannels()){
                    if(areas.getData().contains(get.getName())){
                        LocationChannel loc = LocationChannel.getChannel(get);
                        ourUser.addRole(loc.role);
                    }
                }
            } catch (Exception E){}
        }
    }
    
    private IRole getRoleIfApplicable(String ID){
        List<IRole> roles = Launcher.client.getRoles();
        
        for(IRole R : roles){
            if(R.getName().equals(""+ID)){
                return R;
            }
        }
        
        //Otherwise, create a role for the new friend!
        
        //GETS THE FIRST GUILD POSSIBLE FOR THIS
        RoleBuilder RB = new RoleBuilder(Launcher.client.getGuilds().get(0));
        
        RB.withName(ID);
        RB.withColor(new Color(oRan.nextInt(255),oRan.nextInt(255),oRan.nextInt(255)));
        
        IRole role = RB.build();
        return role;
    }
    
    public boolean hasEmoji(String pool, int num){
        int numba = 0;
        
        for(String s : fish.getData().keySet()){
            Emoji E = EmojiManager.getByUnicode(s);
            if(EmojiDex.descriptors.get(pool).contains(E)){
                numba+= fish.getData().get(s);
            }
        }
        
        return numba >= num;
    }
    
    public void removeEmoji(String pool, int num){
        
        ArrayList<String> rand = new ArrayList<>();
        
        for(String s : fish.getData().keySet()){
            Emoji E = EmojiManager.getByUnicode(s);
            if(EmojiDex.descriptors.get(pool).contains(E)){
                rand.add(s);
            }
        }
        
        int tries = 0;
        for(int i=0; i<num && tries < 1000;++i){
            String get = rand.get(oRan.nextInt(rand.size()));
            if(fish.getData().get(get) > 0){
                fish.getData().put(get,fish.getData().get(get)-1);
            } else{
                i-=1;
                ++tries;
            }
        }
        fish.write();
    }
    
    public void giveItem(Item I, long num){
        if(this.inventory.getData().get(I.ID)==null){
            this.inventory.getData().put(I.ID,0L);
            this.inventory.write();
        }
        num += this.inventory.getData().get(I.ID);
        this.inventory.getData().put(I.ID,num);
        this.inventory.write();
    }
}
