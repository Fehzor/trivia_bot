/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games;

import Bot.Fields.UserData;
import static Bot.SuperRandom.oRan;
import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import org.json.JSONObject;

/**
 *
 * @author FF6EB4
 */
public class EmojiDex {
    private EmojiDex ED = new EmojiDex();
    
    public static HashMap<String,ArrayList<Emoji>> descriptors = new HashMap<>();
    public static HashMap<Emoji,ArrayList<String>> descriptors2 = new HashMap<>();
    public static HashMap<Emoji,Integer> pools = new HashMap<>();
    public static HashMap<String, ArrayList<Emoji>> categories = new HashMap<>();
    public static HashMap<Emoji,String> categories2 = new HashMap<>();
    public static HashMap<Emoji,Boolean> fitzpatrick = new HashMap<>();
    
    public static ArrayList<Emoji> emojis = loadEmoji();
    public static ArrayList<Emoji> loadEmoji(){
        ArrayList<Emoji> ret = new ArrayList<>();
        try{
            JSONObject emojidex;
            File F = new File("resources/emoji.json");
            
            Scanner oScan = new Scanner(F);
            
            String get = "";
            
            while(oScan.hasNextLine()){
                get+=oScan.nextLine()+"\n";
            }
            
            emojidex = new JSONObject(get);
            
            Iterator<?> keys = emojidex.keys();

            while( keys.hasNext() ) {
                String key = (String)keys.next();
                if ( emojidex.get(key) instanceof JSONObject ) {
                    JSONObject entry = (JSONObject)emojidex.get(key);
                    
                    Emoji E = EmojiManager.getByUnicode((String)entry.get("char"));
                    ret.add(E);
                    
                    fitzpatrick.put(E, entry.getBoolean("fitzpatrick_scale"));
                    
                    String cat = entry.getString("category");
                    if(categories.containsKey(cat)){
                        categories.get(cat).add(E);
                        
                    } else {
                        categories.put(cat, new ArrayList<>());
                        categories.get(cat).add(E);
                    }
                    
                    categories2.put(E,cat);
                    
                    
                    int poo = 0;
                    ArrayList<String> d2 = new ArrayList<>();
                    for (Object o : entry.getJSONArray("keywords")){
                        poo++;
                        String s = (String)o;
                        d2.add(s);
                        if(descriptors.containsKey(s)){
                            descriptors.get(s).add(E);
                        } else {
                            descriptors.put(s, new ArrayList<>());
                            descriptors.get(s).add(E);
                        }
                    }
                    pools.put(E,poo);
                    descriptors2.put(E,d2);
                }
            }
        
        } catch (Exception E){}
        
        
        System.out.println(descriptors);
        
        return ret;
    }
    
    public static void addEmoji(UserData UD, String E){
        if(UD.fish.getData().containsKey(E)){
            UD.fish.getData().put(E,UD.fish.getData().get(E)+1);
        } else {
            UD.fish.getData().put(E,1);
        }
    }
    
    public static void addEmoji(String cat, UserData UD){
        try{
            if(EmojiDex.emojis.size()==0){
                EmojiDex.emojis = EmojiDex.loadEmoji();
            }

            ArrayList<Emoji> poo = EmojiDex.descriptors.get(cat);

            Emoji E = poo.get(oRan.nextInt(poo.size()));
            addEmoji(UD,E.getUnicode());
        } catch (Exception E){

            Emoji F = EmojiManager.getForAlias("boot");
            addEmoji(UD,F.getUnicode());
        }
    }
    
    /////////////////OLD FISHING STUFF/////////////
    /*
    public static ArrayList<Emoji> fishingEmoji;
    private static ArrayList<Emoji> setupFishes(){
        ArrayList<Emoji> ret = new ArrayList<>();
        Emoji E;
        
        E=EmojiManager.getForAlias("shrimp");
        ret.add(E);
        assignFITypes(E,new int[]{1});
        assignBaitTypes(E,new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13});
        assignDepths(E,50,50);
        assignRarity(E,1000, CURRENCY_LOLS);
        
        Emoji a = EmojiManager.getForAlias("fried_shrimp");
        Emoji b = EmojiManager.getForAlias("crab");
        Emoji c = EmojiManager.getForAlias("snail");
        Emoji d = EmojiManager.getForAlias("droplet");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        E=EmojiManager.getForAlias("thunder_cloud_rain");
        ret.add(E);
        assignFITypes(E,new int[]{1});
        assignBaitTypes(E,new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13});
        assignDepths(E,-50,50);
        assignRarity(E,1000, CURRENCY_LOLS);
        
        a = EmojiManager.getForAlias("fog");
        b = EmojiManager.getForAlias("umbrella");
        c = EmojiManager.getForAlias("cloud_rain");
        d = EmojiManager.getForAlias("cloud");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        //SHOES
        
        E=EmojiManager.getForAlias("boot");
        ret.add(E);
        assignFITypes(E,new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13});
        assignBaitTypes(E,new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13});
        assignDepths(E,0,100);
        assignRarity(E,35, CURRENCY_ORBS);
        
        a = EmojiManager.getForAlias("sandal");
        b = EmojiManager.getForAlias("high_heel");
        c = EmojiManager.getForAlias("shoe");
        d = EmojiManager.getForAlias("athletic_shoe");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        
        //SMILEYS
        
        E = EmojiManager.getForAlias("grinning");
        assignFITypes(E,new int[]{0,6});
        assignBaitTypes(E,new int[]{0,1,3});
        assignDepths(E,50,25);
        assignRarity(E,200,CURRENCY_TRIVIA);
        ret.add(E);
        
        a = EmojiManager.getForAlias("smiley");
        b = EmojiManager.getForAlias("flushed");
        c = EmojiManager.getForAlias("pensive");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        
        createTier(E,new Emoji[]{a,b,c});
        
        //MORE SMILEYS; FLYING ONES
        
        E = EmojiManager.getForAlias("joy");
        assignFITypes(E,new int[]{0,3});
        assignBaitTypes(E,new int[]{0,1,3});
        assignDepths(E,-5,20);
        assignRarity(E,300,CURRENCY_TRIVIA);
        ret.add(E);
        
        a = EmojiManager.getForAlias("kissing_smiling_eyes");
        b = EmojiManager.getForAlias("heart_eyes");
        c = EmojiManager.getForAlias("bride_with_veil");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        
        createTier(E,new Emoji[]{a,b,c});
        
        //THE HEAVENS WITH A BOX
        
        E = EmojiManager.getForAlias("alien");
        assignFITypes(E,new int[]{0,1});
        assignBaitTypes(E,new int[]{0,1,3});
        assignDepths(E,-95,5);
        assignRarity(E,70,CURRENCY_TRIVIA);
        ret.add(E);
        
        a = EmojiManager.getForAlias("zzz");
        b = EmojiManager.getForAlias("lips");
        c = EmojiManager.getForAlias("star2");
        d = EmojiManager.getForAlias("star");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        //SKIES = BIRDS
        
        E = EmojiManager.getForAlias("bird");
        assignFITypes(E,new int[]{1,2,3});
        assignBaitTypes(E,new int[]{0,1,2});
        assignDepths(E,40,25);
        assignRarity(E,200,CURRENCY_ORBS);
        ret.add(E);
        
        a = EmojiManager.getForAlias("hatched_chick");
        b = EmojiManager.getForAlias("chicken");
        c = EmojiManager.getForAlias("baby_chick");
        d = EmojiManager.getForAlias("rooster");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        //Harpoon stuff
        
        E = EmojiManager.getForAlias("elephant");
        assignFITypes(E,new int[]{4,7});
        assignBaitTypes(E,new int[]{0,1,3});
        assignDepths(E,0,5);
        assignRarity(E,300,CURRENCY_ORBS);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("sheep");
        b = EmojiManager.getForAlias("monkey");
        c = EmojiManager.getForAlias("ram");
        d = EmojiManager.getForAlias("money_mouth");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        //Harpoon stuff
        
        E = EmojiManager.getForAlias("space_invader");
        assignFITypes(E,new int[]{4,7});
        assignBaitTypes(E,new int[]{2,4});
        assignDepths(E,0,50);
        assignRarity(E,300,CURRENCY_LOLS);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("boar");
        b = EmojiManager.getForAlias("dragon");
        c = EmojiManager.getForAlias("water_buffalo");
        d = EmojiManager.getForAlias("tiger");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        E = EmojiManager.getForAlias("money_with_wings");
        assignFITypes(E,new int[]{0,1,2,3});
        assignBaitTypes(E,new int[]{0,4});
        assignDepths(E,-50,25);
        assignRarity(E,35,CURRENCY_BLOCKS);
        ret.add(E);
        
        // FISH AND WATER STUFF
        
        E = EmojiManager.getForAlias("fish");
        assignFITypes(E,new int[]{0,1,2,3});
        assignBaitTypes(E,new int[]{0,1,2});
        assignDepths(E,0,50);
        assignRarity(E,300,CURRENCY_BLOCKS);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("blowfish");
        b = EmojiManager.getForAlias("tropical_fish");
        c = EmojiManager.getForAlias("snake");
        d = EmojiManager.getForAlias("surfer");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        assignRarity(d,5,CURRENCY_TRIVIA);
        
        //Bottom Dwellers
        
        E = EmojiManager.getForAlias("crocodile");
        assignFITypes(E,new int[]{4,7});
        assignBaitTypes(E,new int[]{2,4});
        assignDepths(E,90,10);
        assignRarity(E,200,CURRENCY_ORBS);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("japanese_goblin");
        b = EmojiManager.getForAlias("imp");
        c = EmojiManager.getForAlias("octopus");
        d = EmojiManager.getForAlias("mute");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        //Plants
        
        E = EmojiManager.getForAlias("herb");
        assignFITypes(E,new int[]{5});
        assignBaitTypes(E,new int[]{0,1,2,3,4});
        assignDepths(E,0,20);
        assignRarity(E,300,CURRENCY_ORBS);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("mushroom");
        b = EmojiManager.getForAlias("palm_tree");
        c = EmojiManager.getForAlias("tulip");
        d = EmojiManager.getForAlias("rose");
        Emoji e = EmojiManager.getForAlias("seedling");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        ret.add(e);
        
        createTier(E,new Emoji[]{a,b,c,d,e});
        
        //High chrejr
        
        E = EmojiManager.getForAlias("full_moon");
        assignFITypes(E,new int[]{5});
        assignBaitTypes(E,new int[]{0,1,2,3,4});
        assignDepths(E,-60,40);
        assignRarity(E,300,CURRENCY_BLOCKS);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("first_quarter_moon");
        b = EmojiManager.getForAlias("waning_crescent_moon");
        c = EmojiManager.getForAlias("sun_with_face");
        d = EmojiManager.getForAlias("stars");
        e = EmojiManager.getForAlias("boom");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        ret.add(e);
        
        createTier(E,new Emoji[]{a,b,c,d,e});
        
        //High chrejr
        
        E = EmojiManager.getForAlias("skull_crossbones");
        assignFITypes(E,new int[]{5});
        assignBaitTypes(E,new int[]{0,1,2,3,4});
        assignDepths(E,60,40);
        assignRarity(E,300,CURRENCY_BLOCKS);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("oil_drum");
        b = EmojiManager.getForAlias("japan");
        c = EmojiManager.getForAlias("low_brightness");
        d = EmojiManager.getForAlias("potato");
        e = EmojiManager.getForAlias("poop");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        ret.add(e);
        
        createTier(E,new Emoji[]{a,b,c,d,e});
        
        E=EmojiManager.getForAlias("bug");
        ret.add(E);
        assignFITypes(E,new int[]{0,5,6});
        assignBaitTypes(E,new int[]{1,3,4});
        assignDepths(E,-10,15);
        assignRarity(E,300,CURRENCY_ORBS);
        
        a = EmojiManager.getForAlias("bee");
        b = EmojiManager.getForAlias("beetle");
        c = EmojiManager.getForAlias("ant");
        d = EmojiManager.getForAlias("spider");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        E=EmojiManager.getForAlias("tongue");
        ret.add(E);
        assignFITypes(E,new int[]{2,6});
        assignBaitTypes(E,new int[]{1,2});
        assignDepths(E,90,15);
        assignRarity(E,100,CURRENCY_TRIVIA);
        
        a = EmojiManager.getForAlias("ear");
        b = EmojiManager.getForAlias("nose");
        c = EmojiManager.getForAlias("eye");
        d = EmojiManager.getForAlias("eyes");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        E=EmojiManager.getForAlias("whale");
        ret.add(E);
        assignFITypes(E,new int[]{1,4});
        assignBaitTypes(E,new int[]{0,5});
        assignDepths(E,10,10);
        assignRarity(E,11,CURRENCY_ORBS);
        
        a = EmojiManager.getForAlias("dolphin");
        b = EmojiManager.getForAlias("shark");
        c = EmojiManager.getForAlias("whale2");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        
        createTier(E,new Emoji[]{a,b,c});
        
        E=EmojiManager.getForAlias("shirt");
        ret.add(E);
        assignFITypes(E,new int[]{2});
        assignBaitTypes(E,new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13});
        assignDepths(E,0,100);
        assignRarity(E,350,CURRENCY_ORBS);
        
        a = EmojiManager.getForAlias("womans_clothes");
        b = EmojiManager.getForAlias("running_shirt_with_sash");
        c = EmojiManager.getForAlias("jeans");
        d = EmojiManager.getForAlias("dress");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        E=EmojiManager.getForAlias("airplane");
        ret.add(E);
        assignFITypes(E,new int[]{1,4});
        assignBaitTypes(E,new int[]{0,5});
        assignDepths(E,-80,20);
        assignRarity(E,15,CURRENCY_BLOCKS);
        
        a = EmojiManager.getForAlias("helicopter");
        b = EmojiManager.getForAlias("small_airplane");
        c = EmojiManager.getForAlias("rocket");
        d = EmojiManager.getForAlias("satellite");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        E=EmojiManager.getForAlias("rat");
        ret.add(E);
        assignFITypes(E,new int[]{0,7});
        assignBaitTypes(E,new int[]{1,5});
        assignDepths(E,10,10);
        assignRarity(E,200,CURRENCY_ORBS);
        
        a = EmojiManager.getForAlias("mouse2");
        b = EmojiManager.getForAlias("rabbit2");
        
        ret.add(a);
        ret.add(b);
        
        createTier(E,new Emoji[]{a,b});
        
        E = EmojiManager.getForAlias("children_crossing");
        assignFITypes(E,new int[]{6});
        assignBaitTypes(E,new int[]{0,3,4,5});
        assignDepths(E,0,15);
        assignRarity(E,300,CURRENCY_TRIVIA);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("girl");
        b = EmojiManager.getForAlias("boy");
        c = EmojiManager.getForAlias("baby");
        d = EmojiManager.getForAlias("cop");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        
        createTier(E,new Emoji[]{a,b,c,d});
        
        E = EmojiManager.getForAlias("scream_cat");
        assignFITypes(E,new int[]{6});
        assignBaitTypes(E,new int[]{1,2});
        assignDepths(E,0,15);
        assignRarity(E,300,CURRENCY_LOLS);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("crying_cat_face");
        b = EmojiManager.getForAlias("confused");
        c = EmojiManager.getForAlias("see_no_evil");
        
        ret.add(a);
        ret.add(b);
        ret.add(c);
        
        createTier(E,new Emoji[]{a,b,c});
        
        E = EmojiManager.getForAlias("angel");
        assignFITypes(E,new int[]{6});
        assignBaitTypes(E,new int[]{0,4,5});
        assignDepths(E,-70,30);
        assignRarity(E,200,CURRENCY_LOLS);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("santa");
        b = EmojiManager.getForAlias("levitate");
        
        ret.add(a);
        ret.add(b);
        
        createTier(E,new Emoji[]{a,b});
        
        E = EmojiManager.getForAlias("ghost");
        assignFITypes(E,new int[]{6,7});
        assignBaitTypes(E,new int[]{0,3,4,5});
        assignDepths(E,70,30);
        assignRarity(E,200,CURRENCY_TRIVIA);
        ret.add(E);
        
        
        a = EmojiManager.getForAlias("skull");
        b = EmojiManager.getForAlias("japanese_ogre");
        
        ret.add(a);
        ret.add(b);
        
        createTier(E,new Emoji[]{a,b});
        
        return ret;
    }
    
    public static final int CURRENCY_TRIVIA = 0;
    public static final int CURRENCY_ORBS = 1;
    public static final int CURRENCY_LOLS = 2;
    public static final int CURRENCY_BLOCKS = 3;
    public static final int CURRENCY_MONUSET = 4;
    
    public static HashMap<Emoji,Integer> currencyType = new HashMap<>();
    public static HashMap<Integer,ArrayList<Emoji>> EbyCurrencyType = new HashMap<>();
    public static HashMap<Emoji,Integer> lowValue = new HashMap<>();
    public static HashMap<Emoji,Integer> highValue = new HashMap<>();
    
    public static HashMap<Integer,ArrayList<Emoji>> FItype2E = new HashMap<>();
    public static HashMap<Emoji,int[]> E2FItype = new HashMap<>();
    
    public static HashMap<Integer,ArrayList<Emoji>> FIbaits2E = new HashMap<>();
    public static HashMap<Emoji,int[]> E2FIbaits = new HashMap<>();
    
    public static HashMap<Integer,ArrayList<Emoji>> Depths2E = new HashMap<>();
    public static HashMap<Emoji,int[]> E2Depths = new HashMap<>();
    
    public static HashMap<Emoji, Integer> rarities = new HashMap<>();
    
    private static void assignBaitTypes(Emoji E, int[] types){
        for(int i : types){
            if(FIbaits2E.containsKey(i)){
                FIbaits2E.get(i).add(E);
            } else {
                FIbaits2E.put(i,new ArrayList<>());
                FIbaits2E.get(i).add(E);
            } 
        }
        
        E2FIbaits.put(E,types);
    }
    private static void assignFITypes(Emoji E, int[] types){
        for(int i : types){
            if(FItype2E.containsKey(i)){
                FItype2E.get(i).add(E);
            } else {
                FItype2E.put(i,new ArrayList<>());
                FItype2E.get(i).add(E);
            } 
        }
        
        E2FItype.put(E,types);
    }
    
    private static void assignDepths(Emoji E, int depth, int variance){
        int start = depth - variance;
        int end = depth + variance;
        
        for(int i = start; i < end; ++i){
            if(Depths2E.containsKey(i)){
                Depths2E.get(i).add(E);
            } else {
                Depths2E.put(i,new ArrayList<>());
                Depths2E.get(i).add(E);
            }
        }
        
        E2Depths.put(E,new int[]{depth,variance});
    }
    
    public static void assignRarity(Emoji E, int rare, int curr){
        rarities.put(E,rare);
        
        if(rare < 20){
            assignCosts(E,curr,50,500);
        } else if(rare < 100){
            assignCosts(E,curr,35,150);
        } else if(rare < 200){
            assignCosts(E,curr,25,70);
        } else if(rare < 300){
            assignCosts(E,curr,15,20);
        } else if(rare < 400){
            assignCosts(E,curr,5,10);
        } else {
            assignCosts(E,curr,1,3);
        }
    }
    
    private static void createTier(Emoji set, Emoji [] tier){
        
        for(Emoji E : tier){
            assignFITypes(E,E2FItype.get(set));
            assignBaitTypes(E,E2FIbaits.get(set));
            assignDepths(E,E2Depths.get(set)[0],E2Depths.get(set)[1]);
            assignRarity(E,rarities.get(set),currencyType.get(set));
        }
    }
    
    public static void assignCosts(Emoji E, int type, int low, int high){
        currencyType.put(E,type);
        lowValue.put(E, low);
        highValue.put(E,high);
        
        if(EbyCurrencyType.containsKey(type)){
            EbyCurrencyType.get(type).add(E);
        } else {
            EbyCurrencyType.put(type, new ArrayList<>());
            EbyCurrencyType.get(type).add(E);
        }
    }
    */
}

//Garbo
        /*
        
        */