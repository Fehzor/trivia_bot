/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Fishing;

import Bot.Fields.Field;
import Games.*;
import Bot.Fields.UserData;
import Bot.Launcher;
import static Bot.SuperRandom.oRan;
import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;

/**
 *
 * @author FF6EB4
 */
public class FishChannel extends GameChannel{
    
    private static final String emojiRegex = "([\\u20a0-\\u32ff\\ud83c\\udc00-\\ud83d\\udeff\\udbb9\\udce5-\\udbb9\\udcee])";
    
    public Field<ArrayList<String>> users;
    public Field<HashMap<String,Long>> starts;
    
    public FishChannel(IChannel chan){
        super(chan);
        this.col = new Color(30,232,178);
        
        users = new Field<>("FISHING","USERS",new ArrayList<>());
        starts = new Field<>("FISHING","STARTS",new HashMap<>());
    }

    
    public void tick(){
        super.tick();
        
        
        IMessage mess = chan.getMessageHistory().get(0);
        UserData UD = UserData.getUD(mess.getAuthor());

        if(mess.getContent().toLowerCase().equals("check")){
            long time = System.currentTimeMillis() - starts.getData().get(UD.ID);
            FishingGizmo G = FishingGizmo.getGizmo(UD.ID);
            
            if(time > G.castTime.getData()){
                time = time - G.castTime.getData();
                int give = 0;
                if(G.type.getData()==0){
                    
                    if(time > G.waitTime.getData()){
                        for(int i = 0; i < G.amount.getData(); ++i){
                            if(oRan.nextInt(1000) < G.catchChance.getData()){
                                starts.getData().put(UD.ID, System.currentTimeMillis());
                                give += 1;
                            } else {
                                starts.getData().put(UD.ID, System.currentTimeMillis());
                                //send(UD.name,"Oh no! It got away!");
                                give += 0;
                            }
                        }
                    }
                }
                if(G.type.getData()==1){
                    if(time > G.waitTime.getData()){
                        starts.getData().put(UD.ID, System.currentTimeMillis());
                        give = (int) (((time*((long)G.catchChance.getData()) / G.waitTime.getData() )/10000L));
                    } else {
                        starts.getData().put(UD.ID, System.currentTimeMillis());
                        send(UD.name,"Not enough time has passed....");
                    }
                }
                
                if(give > 0){
                    String gString = "";
                    for(int i=0;i<give;++i){
                        try{
                            if(EmojiDex.emojis.size()==0){
                                EmojiDex.emojis = EmojiDex.loadEmoji();
                            }
                            
                            ArrayList<Emoji> poo = EmojiDex.descriptors.get(G.pool.getData());
                            
                            Emoji E = poo.get(oRan.nextInt(poo.size()));
                            
                            gString+=E.getUnicode();
                            
                            
                            //addEmoji(UD,E);
                        } catch (Exception E){
                            
                            Emoji F = EmojiManager.getForAlias("boot");
                            gString+=F.getUnicode();
                        }
                        
                        Matcher matchEmo;
                        String gnu = "";
                        matchEmo = Pattern.compile(emojiRegex).matcher(gString);
                        while (matchEmo.find()) {
                            String emoji = matchEmo.group();
                            
                            gnu += emoji;
                            EmojiDex.addEmoji(UD,emoji);
                        }
                        
                    }

                    send(UD.name,"Congratulations! You've caught "+gString+"!!!");
                } else {
                    starts.getData().put(UD.ID, System.currentTimeMillis());
                    send(UD.name,"You caught zero fish! They got away!");
                }
            } else{
                starts.getData().put(UD.ID, System.currentTimeMillis());
                send(UD.name,"You quickly pull your gizmo back in to discover nothing of interest....");
            }
        }
        
        if(mess.getContent().split(" ",2)[0].toLowerCase().equals("upgrade")){
            try{
                if(UD.hasEmoji("fish", 50)){
                    FishingGizmo G = FishingGizmo.getGizmo(UD.ID);
                    if(G.upgrade(mess.getContent().split(" ",2)[1].toLowerCase()) == true ){
                        send(UD.name,"SUCCESS!");
                        UD.removeEmoji("fish", 50);
                    } else {
                        send(UD.name,"Fishing part not recognized: "+mess.getContent().split(" ",2)[1]);
                    }
                } else {
                    send(UD.name,"Upgrading your gizmo costs 50 fish emoj!");
                }
            }catch (Exception E){}
        }
        
        if(mess.getContent().split(" ",2)[0].toLowerCase().equals("pool")){
            FishingGizmo G = FishingGizmo.getGizmo(UD.ID);
            String get = mess.getContent().split(" ",2)[1].toLowerCase();
            if(G.pools.getData().contains(get)){
                G.pool.writeData(mess.getContent().split(" ",2)[1].toLowerCase());
                send(UD.name,"Pool has been set");
            } else{
                String cats = "";
                for(String S : G.pools.getData()){
                    cats+=S+"; ";
                }
                send(UD.name,"Pool must be one of: "+cats);
            }
        }
        
        if(mess.getContent().split(" ",2)[0].toLowerCase().equals("status")){
            FishingGizmo G = FishingGizmo.getGizmo(UD.ID);
            long time = System.currentTimeMillis() - starts.getData().get(UD.ID);
            
            String tsend = G+"\n";
            tsend+="Time Spent Fishing: "+time/1000+" seconds\n";
            
            send(UD.name,tsend);
        }
        
        if(mess.getContent().split(" ",2)[0].toLowerCase().equals("active")){
            FishingGizmo G = FishingGizmo.getGizmo(UD.ID);
            G.type.writeData(0);
            send(UD.name,"Set to active");
        }
        
        if(mess.getContent().split(" ",2)[0].toLowerCase().equals("passive")){
            FishingGizmo G = FishingGizmo.getGizmo(UD.ID);
            G.type.writeData(1);
            send(UD.name,"Set to passive");
        }
        
        starts.write();
        UD.fish.write();
        
    }
    
    

    
    public void addUser(UserData UD){
        super.addUser(UD);
        
        if(!users.getData().contains(UD.ID)){
            starts.getData().put(UD.ID, System.currentTimeMillis());
            send(UD.name,"Hellllllllo and whalecome to the wilde world of fishing!\n"
                    + "Type check to check your rod (and reset progress)\n"
                    + "Type upgrade with a fishing part to upgrade your gizmo\n"
                    + "Type status to check your status and current gizmo\n"
                    + "Type active or passive to switch styles");
            starts.write();
            
            users.getData().add(UD.ID);
            users.write();
        }
        
    }
    
    
    
}
