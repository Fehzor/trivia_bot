/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Fishing;

import Bot.Fields.Field;
import Bot.Fields.UserData;
import Bot.Launcher;
import static Bot.SuperRandom.oRan;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author FF6EB4
 */
public class FishingGizmo {
    
    
    public Field<Integer> amount;
    public Field<Long> castTime;
    public Field<Long> waitTime;
    public Field<Integer> catchChance;//Out of 1000
    public Field<ArrayList<String>> pools;
    
    public Field<Integer> type;
    public Field<String> pool;
    
    public final int DEFAULT_AMOUNT = 1;
    public final long DEFAULT_CAST = 1000*60;
    public final long DEFAULT_WAIT = 1000*60*7;
    public final int DEFAULT_CHANCE = 300;
    
    private static HashMap<String,FishingGizmo> gizmos = new HashMap<>();
    public static FishingGizmo getGizmo(String UDID){
        if(gizmos.containsKey(UDID)){
            return gizmos.get(UDID);
        }
        return new FishingGizmo(UDID);
    }
    
    public FishingGizmo(String UDID){
        amount = new Field<>("FISHING_NUM",UDID,DEFAULT_AMOUNT);
        castTime = new Field<>("FISHING_CAST",UDID,DEFAULT_CAST);
        waitTime = new Field<>("FISHING_WAIT",UDID,DEFAULT_WAIT);
        catchChance = new Field<>("FISHING_CHANCE",UDID,DEFAULT_CHANCE);
        
        pool = new Field<>("FISHING_POOL",UDID,"fish");
        type = new Field<>("FISHING_TYPE",UDID,0);
        
        gizmos.put(UDID, this);
        
        ArrayList P = new ArrayList<>();
        P.add("fish");
        P.add("ocean");
        pools = new Field<>("FISHING_POOLS",UDID,P);
    }
    
    public String toString(){
        String ret = "**-----Fishing Gizmo-----**\n";
        ret+="**Type:** "+getTypeInfo()+"\n";
        if(type.getData()==0){
            ret+="**Minimum Time:** "+((castTime.getData()+waitTime.getData())/(1000))+" seconds for up to "+amount.getData()+" fish\n";
            ret+="**Odds of catching a fish:** "+(catchChance.getData()/10)+"%\n";
        } else if(type.getData()==1){
            ret+="**Minimum Time:** "+((castTime.getData()+waitTime.getData())/(100))+" seconds\n";
            ret+="**Average Fish/Time:** "+((double)catchChance.getData()/(double)1000)*(double)amount.getData()+"\n";
        }
        
        String cats = "";
        for(String S : pools.getData()){
            cats+=S+"; ";
        }
        
        ret+="**Current Pool:** "+pool.getData()+" \n"
        + "*(Use pool <category> to change pools- category should be one of: "+cats+")*\n";
        
        return ret;
    }
    
    private String getTypeInfo(){
        if(type.getData() == 0){
            return "Active";
        }
        if(type.getData() == 1){
            return "Passive";
        }
        return "Other";
    }
    
    public boolean upgrade(String S){
        
        switch(S){
            case "hook":
            case "bobber":
            case "time":
            case "sinker":
            case "rod":    
                investPoints(6000,waitTime);
                return true;
                
            case "line":
            case "reel":
            case "poll":
            case "pole":
            case "boat":
            case "beer":
            case "cast":    
                investPoints(6000,castTime);
                return true;
                
            case "net":
            case "large net":
            case "lure":
            case "odds":
            case "bait":
            case "worm":
                investPoints(60,catchChance);
                return true;
            case "size":
            case "bread":
            case "shotgun":
            case "harpoon":
            case "amount":
                investPoints(7,amount);
                return true;
            case "pools":
                if(this.pools.getData().contains("shoes")){
                    Launcher.send("You can only upgrade pools once!");
                    return false;
                } else {
                    this.pools.getData().add("shoes");
                    this.pools.write();
                    return true;
                }
        }

        
        return false;
    }
    
    private void investPoints(int points, Field which){
        if(which.equals(amount)){
            for(int i = 0; i < points; ++i){
                if(oRan.nextInt(10+amount.getData())==0){
                    amount.append(1);
                }
            }
        }
        if(which.equals(castTime)){
            long time = castTime.getData();
            
            double ans = ((double)(time*time))/((double)(time+points));
            time = (long)Math.floor(ans);
            
            castTime.writeData(time);
        }
        if(which.equals(waitTime)){
            long time = waitTime.getData();
            
            double ans = ((double)(time*time))/((double)(time+points));
            time = (long)Math.floor(ans);
            
            waitTime.writeData(time);
        }
        if(which.equals(catchChance)){
            int odds = catchChance.getData();
            
            double sqrt = Math.sqrt(odds);
            double sqrt2 = Math.sqrt(odds+points);
            
            odds = (int)Math.ceil(sqrt*sqrt2);
            
            catchChance.writeData(odds);
        }
    }
}
