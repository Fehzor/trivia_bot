/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games;

import Bot.Fields.UserData;
import Bot.Launcher;
import static Bot.SuperRandom.oRan;
import java.awt.Color;
import java.util.ArrayList;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

/**
 *
 * @author FF6EB4
 */
public class GameChannel {
    
    public static final long TIME_OUT = 1000 * 60 * 3; // 3 minutes
    public Color col=new Color(oRan.nextInt(255),oRan.nextInt(255),oRan.nextInt(255));;
    
    public IChannel chan;
    
    public ArrayList<UserData> userList = new ArrayList<>();
    
    public GameChannel(IChannel chan){
        this.chan = chan;
    }
    
    public void tick(){
        inactivity();
    }
    
    public void send(String title,String desc){
       
       try{
           //System.out.println("BUILDER");
           EmbedBuilder builder = new EmbedBuilder();


           //System.out.println("COLOR 2");
           builder.withColor(col.getRed(), col.getGreen(), col.getBlue());

           //System.out.println("DESCRIPTION");           
           builder.withTitle(title.toUpperCase()+":");

           builder.appendDescription(desc);

           //System.out.println("SENDING");
           Launcher.send(builder, chan);
       } catch(Exception E){E.printStackTrace();}
   }
    
    //Checks for inactivity
    public void inactivity(){
        long now = System.currentTimeMillis();
        
        for(int i = 0; i < userList.size(); ++i){
            if(now - userList.get(userList.size()-1).lastMessage.getData() > TIME_OUT){
                userList.remove(userList.size()-1);
            }
        }
    }
    
    public void addUser(UserData UD){
        if(this.userList.contains(UD)){
            return;
        } else {
            this.userList.add(UD);
        }
    }
}
