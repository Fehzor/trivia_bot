/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games;

import Games.Trivia.Trivia;
import Games.Trivia.TriviaChannel;
import Bot.Fields.UserData;
import Bot.Launcher;
import static Bot.SuperRandom.oRan;
import Games.Fishing.FishChannel;
import Games.Minion.MinionChannel;
import Games.PacMan.PacChannel;
import Games.Trivia.Math.MathTrivia;
import java.util.ArrayList;
import java.util.HashMap;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;

/**
 *
 * @author FF6EB4
 */
public class GameManager {
    
    public static HashMap<IChannel, GameChannel> channels = initiate();
    private static HashMap<IChannel,GameChannel> initiate(){
        HashMap<IChannel, GameChannel> ret = new HashMap<>();
        
        TriviaChannel math = math();
        ret.put(math.chan, math);
        
        PacChannel pc = pac();
        ret.put(pc.chan,pc);
        
        FishChannel FC = fish();
        ret.put(FC.chan,FC);
        
        MinionChannel MC = mini();
        MinionChannel.MC=MC;
        ret.put(MC.chan,MC);
        
        //*/
        return ret;
    }
    
    public static PacChannel pac(){
        PacChannel PC = new PacChannel(Launcher.client.getChannelByID(454856316133965824L));
        
        return PC;
    }
    
    
    public static TriviaChannel math(){
        TriviaChannel chan = new TriviaChannel(Launcher.client.getChannelByID(454800775592542220L), new MathTrivia());
        
        return chan;
    }
    
    public static FishChannel fish(){
        return new FishChannel(Launcher.client.getChannelByID(455808282817331201L));
    }
    
    public static MinionChannel mini(){
        return new MinionChannel(Launcher.client.getChannelByID(464907419844214785L));
    }
}
