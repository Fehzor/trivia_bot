/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion;

import Games.Trivia.*;
import Bot.Fields.UserData;
import Bot.Launcher;
import Games.GameChannel;
import static Bot.SuperRandom.oRan;
import Games.EmojiDex;
import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;
import Games.Minion.model.MShop;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

/**
 *
 * @author FF6EB4
 */
public class MinionChannel extends GameChannel{
    public static MinionChannel MC;
    
    public long TIMEOUT = 1000 * 60 * 10 ;
    public HashMap<String,Long> last = new HashMap<>();
    
    public HashMap<String,MData> datas = new HashMap<>();
    public HashMap<String,MDeck> decks = new HashMap<>();
    
    MShop S;
    
    public MinionChannel(IChannel chan){
        super(chan);
        
        col = new Color(244,227,36);
        S = new MShop(userList.size());   
        
        Thread T = new Thread(){
            public void run(){
                while(true){
                    try{
                        Thread.sleep(TIMEOUT);
                    } catch (Exception E){}
                    
                    for(UserData UD : userList){
                        if(System.currentTimeMillis() - last.get(UD.ID) > 30 * 1000){
                            
                           datas.put(UD.ID, decks.get(UD.ID).deal(datas.get(UD.ID),UD));
                           
                           
                        }
                    }
                    if(userList.size() > 0){
                        yell("A NEW ROUND", "It's the next round! Your hands have been refreshed!");
                        String send = "**SHOP CONTAINS:** \n";
                        for(ArrayList<MCard> CL : S.shop){
                            if(CL.size() == 0){
                                send+="**SOLD OUT**: This item was in stock but no longer is.\n";
                            } else {
                                send += ""+CL.get(0)+"*In Stock: "+CL.size()+"*\n";
                            }
                        }
                        yell("**SHOP**",send);
                    }
                }
            }
        }; T.start();
    }
    
    
    
    public void tick(){
        super.tick();
        
        IMessage mess = chan.getMessageHistory().get(0);
        UserData UD = UserData.getUD(mess.getAuthor());
        
        MData dat = datas.get(UD.ID);
        
        String get = mess.getContent();
        
        
        String args = "";
        try{
            args = get.split(" ",2)[1].toLowerCase();
        } catch (Exception E){}
        
        String com = get.split(" ", 2)[0].toLowerCase();
        
        if(com.equals("play")){
            String s = dat.play(args);
            send(UD.name,s);
        }
        
        if(com.equals("hand")){
            String send = "**CARDS REMAINING:** \n";
            for(MCard C : dat.cards){
                send += ""+C;
            }
            send += "\n**CARDS PLAYED OR BOUGHT:** \n";
            for(MCard C : dat.played){
                send += ""+C;
            }
            send+="\n**STATUS:**\n"
                    + "Money: "+dat.money+"\n"
                    + "Actions Remaining: "+dat.action+"\n"
                    + "Buys: "+dat.buy+"\n";
            
            this.send(UD,send);
        }
        
        if(com.equals("buy")){
            MCard MC = S.buy(args);
            if(MC != null){
                if(dat.buy > 0 && dat.money >= MC.value){
                    dat.played.add(MC);
                    dat.buy -= 1;
                    dat.money -= MC.value;
                    yell(UD.name,"Bought "+MC.name+"!");
                } else {
                    yell(UD.name,"Not enough buys or money!");
                }
            } else {
                yell(UD.name,"That card is out or doesn't exist!");
            }
            
            if(S.empty >= 3){
                reset();
                return;
            }
        }
        
        if(com.equals("tutorial")){
            Launcher.PM("This is a variant of the game dominion: https://www.youtube.com/watch?v=5jNGpgdMums&feature=youtu.be \n"
                    + "Major differences include:\n"
                    + "+The game is played all at once rather than turn by turn. A new turn happens every couple turns."
                    + "+The game's cards have differing abilities\n"
                    + "+The game's theme\n"
                    + "+The game ends when 3 piles are empty.\n",Long.parseLong(UD.ID));
        }
    }
    
    public void addUser(UserData UD){
        super.addUser(UD);
        
        yell(UD.name+" has joined the game!","Commands are as follows:\n"
                + "*hand = view your hand*\n"
                + "*buy <card> = buy a card from the shop*\n"
                + "*play <card> = play a card from your hand*\n"
                + "Type tutorial for info on how to play!");
        
        last.put(UD.ID, 0L);
        decks.put(UD.ID, new MDeck());
        datas.put(UD.ID, decks.get(UD.ID).deal(null,UD));
        
        if(userList.size() == 1){
            String send = "**SHOP CONTAINS:** \n";
            for(ArrayList<MCard> CL : S.shop){
                if(CL.size() == 0){
                    send+="SOLD OUT\n";
                } else {
                    send += ""+CL.get(0)+"*In Stock: "+CL.size()+"*\n";
                }
            }
            yell("**SHOP**",send);
        }
    }
    
    public void reset(){
        int highest = 0;
        UserData winner = null;
        for(UserData UD : userList){
            MData hand = datas.get(UD.ID);
            MDeck deck = decks.get(UD.ID);
            
            int vp = deck.VP(hand);
            
            if(vp > highest){
                highest = vp;
                winner = UD;
            }
        }
        int ran = oRan.nextInt(1000);
        int prizeSize = userList.size();
        if(ran < 600){
            for(int i = 0; i < 100*prizeSize; ++i){
                EmojiDex.addEmoji("space", winner);
            }
            yell(winner.name +" has won with "+highest+" VP! +"+100*prizeSize+" space emoji!","^");
        } else if(ran < 605){
            for(int i = 0; i < 20*prizeSize; ++i){
                EmojiDex.addEmoji("potato", winner);
                yell(winner.name +" has won with "+highest+" VP! +"+20*prizeSize+"  Potato Emoji!","^");
            }
        } else {
            for(int i = 0; i < 5*prizeSize; ++i){
                EmojiDex.addEmoji("building", winner);
                yell(winner.name +" has won with "+highest+" VP! +"+5*prizeSize+"  Building Emoji!","^");
            }
        }
        
        userList = new ArrayList<>();
        S = new MShop(userList.size());
        datas = new HashMap<>();
        decks = new HashMap<>();
    }
    
    public void send(UserData UD, String two){
        if(userList.size()>3){
            Launcher.PM(two, Long.parseLong(UD.ID));
        } else {
            super.send(UD.name, two);
        }
    }
    public void yell(String one, String two){
        super.send(one, two);
    }
    
}
