/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model;

import static Bot.SuperRandom.oRan;
import Games.EmojiDex;
import Games.Minion.MinionChannel;

/**
 *
 * @author FF6EB4
 */
public class MCard {
    public int value;
    public int victory;
    
    public String name;
    public String description;
    
    public String category = "building";
    
    public void passive(MData data, MDeck deck){
        
    }
    
    public void active(MData data, MDeck deck){
        if(oRan.nextInt(100)<20){
            EmojiDex.addEmoji(category, data.UD);
            MinionChannel.MC.send(data.UD.name,"+1 "+category+" emoji!");
        }
    }
    
    public String toString(){
        String ret = "**"+name+": ** Shop Value: "+value+"";
        if(victory > 0 ){
            ret+="; Victory Points: "+victory+"\n";
            ret+="*"+description+"*\n"; 
        } else {
            ret+="\n*"+description+"*\n"; 
        }
        
        return ret;
    }
    
    public MCard clone(){
        return new MCard();
    }
    
    
}
