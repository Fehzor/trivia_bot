/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model;

import Bot.Fields.UserData;
import java.util.ArrayList;

/**
 *
 * @author FF6EB4
 */
public class MData {
    public int buy = 1;
    public int action = 1;
    public int money = 0;
    public ArrayList<MCard> cards = new ArrayList<>();
    public ArrayList<MCard> played = new ArrayList<>();
    MDeck deck;
    UserData UD;
    
    public MData(MDeck deck,UserData UD){
        this.deck = deck;
        this.UD = UD;
    }
    
    public String play(String args){
        if(action > 0){
            for(MCard C : cards){
                if(C.name.toLowerCase().equals(args)){
                    C.active(this,deck);
                    cards.remove(C);
                    played.add(C);
                    this.action -= 1;
                    return "Card has been played.";
                }
            }
            return "Card not found.";
        }
        return "Action points depleted- you start with but one action point each turn.";
    }
}
