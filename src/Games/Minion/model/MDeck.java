/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model;

import Bot.Fields.UserData;
import Games.Minion.model.cards.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author FF6EB4
 */
public class MDeck {
    public ArrayList<MCard> cards = new ArrayList<>();
    public ArrayList<MCard> discard = new ArrayList<>();
    
    public MDeck(){
        cards.add(new One());
        cards.add(new One());
        cards.add(new One());
        cards.add(new One());        
        cards.add(new One());
        cards.add(new One());
        cards.add(new One());

        cards.add(new Moon());
        cards.add(new Moon());
        cards.add(new Moon());
        
        Collections.shuffle(cards);
    }
    
    public MData deal(MData old, UserData UD){
        if(old != null){
            discard.addAll(old.cards);
            discard.addAll(old.played);
        }
        
        MData hand = new MData(this, UD);
        for(int i = 0; i < 5; ++i){
            hand.cards.add(draw(hand));
        }
        return hand;
    }
    
    public MCard draw(MData hand){
            if(cards.size() > 0){
                MCard MC = cards.remove(0);
                MC.passive(hand, this);
                
                return MC;
            } else if(discard.size()>0){
                cards = discard;
                discard = new ArrayList<>();
                Collections.shuffle(cards);
                
                MCard MC = cards.remove(0);
                MC.passive(hand, this);
                return MC;
            } else {
                return null;
            }
    }
    
    public int VP(MData hand){
        int ret = 0;
        
        for(MCard MC : cards){
            ret += MC.victory;
        }
        for(MCard MC : discard){
            ret += MC.victory;
        }
        for(MCard MC : hand.cards){
            ret += MC.victory;
        }
        for(MCard MC : hand.played){
            ret += MC.victory;
        }
        
        return ret;
    }
}
