/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model;

import static Bot.SuperRandom.oRan;
import Games.Minion.model.cards.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author FF6EB4
 */
public class MShop {
    public ArrayList<ArrayList<MCard>> shop = new ArrayList<>();
    public int empty = 0;
    
    private ArrayList<MCard> all = new ArrayList<>();
    
    int size = 0;
    public MShop(int size){
        this.size = size;
        
        //CARDS THAT ARE RANDOMIZED
        all.add(new Pillage());
        all.add(new DysonSphere());
        all.add(new Rape());
        all.add(new Casino());
        all.add(new Power());
        all.add(new Influence());
        all.add(new Investment());
        all.add(new Politician());
        all.add(new Sanctions());
        all.add(new SpaceElevator());
        all.add(new PaydayLoan());
        all.add(new VultureCapitalism());
        all.add(new Slavery());
        all.add(new FTL());
        all.add(new Suicide());
        all.add(new Advertising());

        //CARDS THAT ARE ALWAYS IN PLAY
        shop.add(new ArrayList<>());
        addSet(new One(),0,60);

        shop.add(new ArrayList<>());
        addSet(new Two(),1,40);

        shop.add(new ArrayList<>());
        addSet(new Three(),2,30);

        shop.add(new ArrayList<>());
        addSet(new Moon(),3,60);

        shop.add(new ArrayList<>());
        addSet(new Planet(),4,40);

        shop.add(new ArrayList<>());
        addSet(new SolarSystem(),5,30);

        ///////////////////////////////////
        //These are the real cards here ///
        ///////////////////////////////////
        
        Collections.shuffle(all);
        for(int i = 6; i < 12; ++i){
            if(all.size()>0){
                shop.add(new ArrayList<>());
                addSet(all.remove(0),i);
            }
        }

    }
    
    
    private void addSet(MCard MC, int index, int many){
        for(int i = 0; i < many; ++i){
            shop.get(index).add(MC.clone());
        }
    }
    
    private void addSet(MCard MC, int index){
        int many = 5;
        switch(MC.value){
            case 0:
                many = 15 + 5*(size-1);
                break;
            case 1:
                many = 12 + 3*(size-1);
                break;
            case 2:
                many = 9 + (size-1);
                break;
            case 3:
            case 4:
                many = 7 + (size-1);
                break;
            case 5:
            case 6:
                many = 5 + (size-1);
                break;
            case 7:
            case 8:
            case 9:
                many = 4 + (size-1);
                break;
            case 10:
            case 11:
                many = 3 + (size-1)/2;
                break;
            default:
                many = 1;
        }
        
        
        for(int i = 0; i < many; ++i){
            shop.get(index).add(MC.clone());
        }
    }
    
    public MCard buy(String param){
        for(ArrayList<MCard> MC : shop){
            if(MC.size() > 0 && MC.get(0).name.toLowerCase().equals(param.toLowerCase())){
                MCard ret = MC.remove(0);
                if(MC.size() <= 0){
                    empty++;
                }
                return ret;
            }
        }
        return null;
    }
    
    
}
