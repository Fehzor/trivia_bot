/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import static Bot.SuperRandom.oRan;
import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Advertising extends MCard{
    int cash = 1;
    public Advertising(){
        value = 4;
        victory = 0;
        name = "Advertising";
        description = "Worth 1-3 money.";
    }
    
    public void passive(MData data, MDeck deck){
        int ran = oRan.nextInt(1000) ;
        if(ran < 200){
            cash = 3;
        } else if (ran < 500){
            cash = 2;
        } else {
            cash = 1;
        }
        description = "Worth 1-3 money: Current value = "+cash;
        data.money += cash;
    }

    
    public MCard clone(){
        return new Advertising();
    }
}
