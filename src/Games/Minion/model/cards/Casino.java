/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import static Bot.SuperRandom.oRan;
import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Casino extends MCard{
    public Casino(){
        value = 11;
        victory = 13;
        name = "Casino";
        description = "Worth 13 VP";
    }
        
    public MCard clone(){
        return new Casino();
    }
}
