/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Curse extends MCard{
    public Curse(){
        value = 0;
        victory = -1;
        name = "Curse";
        description = "Worth -1 victory points. Accomplishes nothing.";
    }
    
    public void passive(MData data, MDeck deck){
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
    }
    
    public MCard clone(){
        return new Curse();
    }
}
