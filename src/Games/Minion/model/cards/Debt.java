/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Debt extends MCard{
    public Debt(){
        value = 0;
        victory = 0;
        name = "Debt";
        description = "Worth -1 money. Accomplishes nothing.";
    }
    
    public void passive(MData data, MDeck deck){
        data.money += -1;
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
    }
    
    public MCard clone(){
        return new Debt();
    }
}
