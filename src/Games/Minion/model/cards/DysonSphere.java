/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class DysonSphere extends MCard{
    public DysonSphere(){
        value = 6;
        victory = 2;
        name = "Dyson Sphere";
        description = "Worth 2 money and 2 victory points.";
    }
    
    public void passive(MData data, MDeck deck){
        data.money += 2;
    }

    
    public MCard clone(){
        return new DysonSphere();
    }
}
