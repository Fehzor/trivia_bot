/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class FTL extends MCard{
    public FTL(){
        value = 7;
        victory = 0;
        name = "Faster Than Light";
        description = "Gain 2 actions and draw 3 cards.";
        category = "vehicle";
    }
    
    public void passive(MData data, MDeck deck){
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
        data.cards.add(deck.draw(data));
        data.cards.add(deck.draw(data));
        data.cards.add(deck.draw(data));
        data.action += 2;
    }
    
    public MCard clone(){
        return new FTL();
    }
}
