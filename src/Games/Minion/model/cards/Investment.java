/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Investment extends MCard{
    public Investment(){
        value = 5;
        victory = 1;
        name = "Investment";
        description = "Worth "+victory+" victory point(s). Gains value when played.";
        category = "money";
    }
    
    public void passive(MData data, MDeck deck){
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
        victory += 1;
        description = "Worth "+victory+" victory point(s). Gains value when played.";
    }
    
    public MCard clone(){
        return new Investment();
    }
}
