/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Moon extends MCard{
    public Moon(){
        value = 2;
        victory = 1;
        name = "Moon";
        description = "Worth 1 victory point.";
    }
    
    public void passive(MData data, MDeck deck){
    }
    
    public void active(MData data, MDeck deck){
    }
    
    public MCard clone(){
        return new Moon();
    }
}
