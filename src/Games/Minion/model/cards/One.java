/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class One extends MCard{
    public One(){
        value = 0;
        victory = 0;
        name = "One";
        description = "Worth 1 monetary unit.";
        category = "money";
    }
    
    public void passive(MData data, MDeck deck){
        data.money++;
    }
    
    public MCard clone(){
        return new One();
    }
}
