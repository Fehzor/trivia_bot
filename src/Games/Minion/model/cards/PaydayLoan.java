/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class PaydayLoan extends MCard{
    
    int cash;
    public PaydayLoan(){
        cash= 4;
        value = 2;
        victory = 0;
        name = "Payday Loan";
        description = "Worth "+cash+" money. Loses monetary value each time you draw it; \nstops at -3.Gains 1 value back when played.";
        category = "people";
    }
    
    public void passive(MData data, MDeck deck){
        data.money += cash;
        description = "Worth "+cash+" money. Gains monetary value when played.";
        if(cash > -3){
            cash -= 1;
        }
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
        cash += 1;
        description = "Worth "+cash+" money. Gains monetary value when played.";
    }
    
    public MCard clone(){
        return new PaydayLoan();
    }
}
