/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Power extends MCard{
    public Power(){
        value = 5;
        victory = 0;
        name = "Power";
        description = "Gain 2 money, 1 action and 1 buy.";
    }
    
    public void passive(MData data, MDeck deck){
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
        data.money += 2;
        data.buy += 1;
        data.action += 1;
    }
    
    public MCard clone(){
        return new Power();
    }
}
