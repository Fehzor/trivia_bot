/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Bot.Fields.UserData;
import Games.Minion.MinionChannel;
import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Rape extends MCard{
    public Rape(){
        value = 6;
        victory = 0;
        name = "Rape";
        description = "Everyone else gains 2 curse to their discard pile.";
        category = "love";
    }
    
    public void passive(MData data, MDeck deck){
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
        MinionChannel MC = MinionChannel.MC;
        
        data.cards.add(deck.draw(data));
        data.cards.add(deck.draw(data));
        
        for(UserData UD : MC.userList){
            MDeck MD = MC.decks.get(UD.ID);
            if( ! MD.equals(deck) ){
                MD.discard.add(new Curse());
                MD.discard.add(new Curse());
            }
        }
    }
    
    public MCard clone(){
        return new Rape();
    }
}
