/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Slavery extends MCard{
    public Slavery(){
        value = 3;
        victory = -2;
        name = "Slavery";
        description = "Worth 4 money and -2 victory points.";
    }
    
    public void passive(MData data, MDeck deck){
        data.money += 4;
    }

    
    public MCard clone(){
        return new Slavery();
    }
}
