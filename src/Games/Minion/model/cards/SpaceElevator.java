/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class SpaceElevator extends MCard{
    public SpaceElevator(){
        value = 6;
        victory = 0;
        name = "Space Elevator";
        description = "Passive: Gain one action; \nOn Play: Actions set to zero; +3 money";
        category = "money";
    }
    
    public void passive(MData data, MDeck deck){
        data.action += 1;
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
        data.action = 0; 
        data.money += 3;
    }
    
    public MCard clone(){
        return new SpaceElevator();
    }
}
