/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Bot.Fields.UserData;
import Games.Minion.MinionChannel;
import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Suicide extends MCard{
    public Suicide(){
        value = 2;
        victory = 0;
        name = "Suicide";
        description = "Permanently remove all debt, curse, payday loan and vulture capitalism cards from your hand. \nDestroys itself when played.";
        category = "love";
    }
    
    public void passive(MData data, MDeck deck){
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
        MinionChannel MC = MinionChannel.MC;
        
        int i = 0;
        while(data.cards.size() > i){
            if(data.cards.get(i).name.equals("Death") ||
                    data.cards.get(i).name.equals("Debt") ||
                    data.cards.get(i).name.equals("Curse") ||
                    data.cards.get(i).name.equals("Payday Loan") ||
                    data.cards.get(i).name.equals("Vulture Capitalism")){
                
                data.cards.remove(i);
                
            } else {
                i++;
            }
        }
        
        data.cards.add(deck.draw(data));

    }
    
    public MCard clone(){
        return new Suicide();
    }
}
