/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Three extends MCard{
    public Three(){
        value = 6;
        victory = 0;
        name = "Three";
        description = "Worth 3 monetary units.";
        category = "money";
    }
    
    public void passive(MData data, MDeck deck){
        data.money++;
        data.money++;
        data.money++;
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
    }
    
    public MCard clone(){
        return new Three();
    }
}
