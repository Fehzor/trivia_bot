/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class Two extends MCard{
    public Two(){
        value = 3;
        victory = 0;
        name = "Two";
        description = "Worth 2 monetary units.";
        category = "money";
    }
    
    public void passive(MData data, MDeck deck){
        data.money++;
        data.money++;
    }
    
    public MCard clone(){
        return new Two();
    }    
}
