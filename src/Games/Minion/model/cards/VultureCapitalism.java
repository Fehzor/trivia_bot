/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Minion.model.cards;

import Games.Minion.model.MCard;
import Games.Minion.model.MData;
import Games.Minion.model.MDeck;

/**
 *
 * @author FF6EB4
 */
public class VultureCapitalism extends MCard{
    
    int cash;
    public VultureCapitalism(){
        cash= 7;
        value = 4;
        victory = 0;
        name = "Vulture Capitalism";
        description = "Worth 7 money when drawn the first time. Becomes worth 0 afterwards.";
    }
    
    public void passive(MData data, MDeck deck){
        data.money += cash;
        description = "Worth "+cash+" money. Becomes worth 0 afterward first draw.";
        if(cash > 0){
            cash = 0;
        }
    }
    
    public void active(MData data, MDeck deck){
        super.active(data, deck);
    }
    
    public MCard clone(){
        return new VultureCapitalism();
    }
}
