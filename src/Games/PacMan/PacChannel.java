/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.PacMan;

import static Bot.SuperRandom.oRan;
import Games.GameChannel;
import Games.PacMan.PacModel.PacModel;
import static Games.PacMan.PacModel.PacModel.LEFT;
import static Games.PacMan.PacModel.PacModel.RIGHT;
import static Games.PacMan.PacModel.PacModel.STRAIGHT;
import java.awt.Color;
import java.util.ArrayList;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;

/**
 *
 * @author FF6EB4
 */
public class PacChannel extends GameChannel{
    
    PacModel PM = new PacModel();
    

    
    long time = 0L;
    
    public static final int speed = 5;
    public int ticks = 0;
    
    public PacChannel(IChannel chan) {
        super(chan);
        this.col= Color.YELLOW;
        runThread();
    }
    
    public void runThread(){
        
        new Thread(){
            public void run(){
                while(true){
                    if(userList.size()>0 && System.currentTimeMillis() - time > 1 * 1000){
                        
                        ticks++;
                        if(ticks >= speed){
                            next(STRAIGHT);
                            ticks = 0;
                        }
                        
                        time = System.currentTimeMillis();
                        
                    } else {
                        Thread.yield();
                    }
                }
            }
        }.start();
    }
    
    public void tick(){
        super.tick();
        
        ticks++;
        if(ticks >= speed){
            next(STRAIGHT);
            ticks = 0;
        }
        
        IMessage mess = chan.getMessageHistory().get(0);
        String message = mess.getContent();
        if(message.toLowerCase().equals("left")){
            next(LEFT);
            ticks = 0;
        }
        if(message.toLowerCase().equals("right")){
            next(RIGHT);
            ticks = 0;
        }
    }
    
    public void next(int direction){
        PM.next(this, direction);
    }
    
    public int score=0;
    
}
