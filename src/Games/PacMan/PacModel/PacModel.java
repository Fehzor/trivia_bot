/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.PacMan.PacModel;

import Bot.Fields.UserData;
import static Bot.SuperRandom.oRan;
import Games.EmojiDex;
import Games.GameChannel;
import Games.PacMan.PacChannel;
import java.util.ArrayList;

/**
 *
 * @author FF6EB4
 */
public class PacModel {
    
    public boolean wall = false;
    public int pow = 0;
    public int level = 1;
    
    public static ArrayList<String> empty_hall = loadEH();
    public static ArrayList<String> loadEH(){
        ArrayList<String> ret = new ArrayList<>();
        
        ret.add("Nothing here; moving along...");
        ret.add("Nothing here; moving along...");
        ret.add("Nothing here; moving along...");
        ret.add("Nothing here; moving along...");
        ret.add("Nothing here; moving along...");
        ret.add("Nothing here; moving along...");
        ret.add("Nothing here; moving along...");
        ret.add("Nothing here; moving along...");
        ret.add("Nothing here; moving along...");
        ret.add("The path is empty.");
        ret.add("The path is empty.");
        ret.add("The path is empty.");
        ret.add("The maze holds no wind in its empty halls.");
        ret.add("But nobody came.");
        ret.add("The hall is a barren wasteland...");
        ret.add("You dance merrily down the empty hall");
        ret.add("There is no exit.");
        ret.add("The orb that was here now leaves an empty feel to the air, as if something is very wrong..");
        
        return ret;
    }
    
    public static ArrayList<String> dot_hall = loadDH();
    public static ArrayList<String> loadDH(){
        ArrayList<String> ret = new ArrayList<>();
        
        ret.add("WAKA WAKA");
        ret.add("WAKA WAKA");
        ret.add("WAKA WAKA");
        ret.add("WAKA WAKA");
        ret.add("WAKA WAKA");
        ret.add("WAKA WAKA");
        ret.add("WAKA WAKA");
        ret.add("WAKA WAKA");
        ret.add("WAKA WAKA");
        ret.add("WAKA WAKA WAKA");
        ret.add("WAKA WAKA WAKA");
        ret.add("WAKA WAKA WAKA");
        ret.add("WAKA WAKA WAKA");
        ret.add("WAKA WAKA WAKA WAKA");
        ret.add("WAKA WAKA WAKA WAKA");
        ret.add("WAKA WAKA WAKA WAKA");
        ret.add("WAKA WAKA WAKA WAKA WAKOFF");
        ret.add("You consume another orb.");
        ret.add("You consume another orb.");
        ret.add("You consume another orb.");
        ret.add("Another orb, another day");
        ret.add("Good orbs, good times. One for everyone for every level!");
        ret.add("You race forward with all your might to consume the strange globes");
        ret.add("A glowing orb lies ahead... you quickly gobble it up");
        
        
        return ret;
    }
    
    public static ArrayList<String> pow_hall = loadPH();
    public static ArrayList<String> loadPH(){
        ArrayList<String> ret = new ArrayList<>();
        
        ret.add("A massive glowing sphere lays ahead.. as you bite into it you feel yourself grow stronger.");
        ret.add("A huge sphere like this... it's worth 5x the smaller ones!");
        ret.add("Upon eating the massive orb you feel an immense heat rise within you...");
        
        return ret;
    }
    
    public static final int NORTH = 0;
    public static final int EAST = 1;
    public static final int SOUTH = 2;
    public static final int WEST = 3;
    
    public static final int RIGHT = 1;
    public static final int STRAIGHT = 0;
    public static final int LEFT = -1;
    
    public static char [][] map = new char[][]{
        {'.','.','.','.','.','.','.','.','.','.','.','.','x','x','.','.','.','.','.','.','.','.','.','.','.','.'},
        {'.','x','x','x','x','.','x','x','x','x','x','.','x','x','.','x','x','x','x','x','.','x','x','x','x','.'},
        {'o','x','x','x','x','.','x','x','x','x','x','.','x','x','.','x','x','x','x','x','.','x','x','x','x','o'},
        {'.','x','x','x','x','.','x','x','x','x','x','.','x','x','.','x','x','x','x','x','.','x','x','x','x','.'},
        {'.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.'},
        {'.','x','x','x','x','.','x','x','.','x','x','x','x','x','x','x','x','.','x','x','.','x','x','x','x','.'},
        {'.','x','x','x','x','.','x','x','.','x','x','x','x','x','x','x','x','.','x','x','.','x','x','x','x','.'},
        {'.','.','.','.','.','.','x','x','.','.','.','.','x','x','.','.','.','.','x','x','.','.','.','.','.','.'},
        {'x','x','x','x','x','.','x','x','x','x','x','.','x','x','.','x','x','x','x','x','.','x','x','x','x','x'},
        {'x','x','x','x','x','.','x','x','x','x','x','.','x','x','.','x','x','x','x','x','.','x','x','x','x','x'},
        {'x','x','x','x','x','.','x','x',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','x','x','.','x','x','x','x','x'},
        {'x','x','x','x','x','.','x','x',' ','x','x','x','x','x','x','x','x',' ','x','x','.','x','x','x','x','x'},
        {'x','x','x','x','x','.','x','x',' ','x','x','x','x','x','x','x','x',' ','x','x','.','x','x','x','x','x'},
        {'A',' ',' ',' ',' ','.',' ',' ',' ','x','x','x','x','x','x','x','x',' ',' ',' ','.',' ',' ',' ',' ','B'},
        {'x','x','x','x','x','.','x','x',' ','x','x','x','x','x','x','x','x',' ','x','x','.','x','x','x','x','x'},
        {'x','x','x','x','x','.','x','x',' ','x','x','x','x','x','x','x','x',' ','x','x','.','x','x','x','x','x'},
        {'x','x','x','x','x','.','x','x',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','x','x','.','x','x','x','x','x'},
        {'x','x','x','x','x','.','x','x','.','x','x','x','x','x','x','x','x','.','x','x','.','x','x','x','x','x'},
        {'x','x','x','x','x','.','x','x','.','x','x','x','x','x','x','x','x','.','x','x','.','x','x','x','x','x'},
        {'.','.','.','.','.','.','.','.','.','.','.','.','x','x','.','.','.','.','.','.','.','.','.','.','.','.'},
        {'.','x','x','x','x','.','x','x','x','x','x','.','x','x','.','x','x','x','x','x','.','x','x','x','x','.'},
        {'.','x','x','x','x','.','x','x','x','x','x','.','x','x','.','x','x','x','x','x','.','x','x','x','x','.'},
        {'o','.','.','x','x','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','x','x','.','.','o'},
        {'x','x','.','x','x','.','x','x','.','x','x','x','x','x','x','x','x','.','x','x','.','x','x','.','x','x'},
        {'x','x','.','x','x','.','x','x','.','x','x','x','x','x','x','x','x','.','x','x','.','x','x','.','x','x'},
        {'.','.','.','.','.','.','x','x','.','.','.','.','x','x','.','.','.','.','x','x','.','.','.','.','.','.'},
        {'.','x','x','x','x','x','x','x','x','x','x','.','x','x','.','x','x','x','x','x','x','x','x','x','x','.'},
        {'.','x','x','x','x','x','x','x','x','x','x','.','x','x','.','x','x','x','x','x','x','x','x','x','x','.'},
        {'.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.'}
    };
    
    public static final int START_direction = WEST;
    public static final int START_row = 22;
    public static final int START_col = 14;
    
    public static final int START_grow = 17;
    public static final int START_gcol = 14;
    
   
    
    public int direction, row, col, gdirection, grow, gcol;
    public char[][] board;
    
    public PacModel(){
        setup();
    }
    
    public void setup(){
        direction = START_direction;
        row = START_row;
        col = START_col;
        grow = START_grow;
        gcol = START_gcol;
        board = new char[map.length][map[0].length];
        for(int r = 0; r < map.length; ++r){
            for(int c = 0; c < map[0].length; ++c){
                board[r][c] = map[r][c];
            }
        }
    }
    
    // -1 = left;
    // 0 = straight;
    // 1 = right;
    public void next(GameChannel GC, int command){
        boolean flag=true;
        for(char[] cols : board){
            for(char c : cols){
                if(c == '.' || c == 'o'){
                    flag=false;
                }
            }
        }
        if(flag){
            GC.send("BONUS", "You feel strangely full as the world around you feels empty... you black out to find yourself in a similar situation. +1 level; +"+100*level+" circle emoji");
            level++;
            for(UserData UD : GC.userList){
                for(int i = 0; i<100*level;++i){
                    EmojiDex.addEmoji("circle", UD);
                }
                //UD.dots.append(100*level);
            }
            setup();
        }
        
        ghost();
        if(row == grow && col == gcol){
            if(pow > 0){
                GC.send("BONUS", "The brightly coloured ghostly figure is no more! +25 circle emoji!");
                for(UserData UD : GC.userList){
                    for(int i = 0 ; i <25; ++i){
                        EmojiDex.addEmoji("circle", UD);
                    }
                }
                grow = START_grow;
                gcol = START_gcol;
            } else if(oRan.nextInt(100) <33){
                GC.send("Dusted","You've met your fate... a red figure consumes your flesh!");
                level=1;
                setup();
                return;
            } else if(oRan.nextInt(100) <50){
                GC.send("Dusted","You've met your fate... a blue figure consumes your flesh!");
                level=1;
                setup();
                return;
            } else {
                GC.send("Dusted","You've met your fate... a pink figure consumes your flesh!");
                level=1;
                setup();
                return;
            }
        }
        
        direction += command;
        if(direction<0)direction=3;
        if(direction>3)direction=0;
        
        char c = 'x';
        if(direction==WEST){
            try{
                c=board[row][col-1];
                if(c == '.' || c == ' '|| c == 'o'){
                    col = col-1;
                }
            } catch (ArrayIndexOutOfBoundsException AIOOBE){
                c='x';
            }
        }
        if(direction==EAST){
            try{
                c=board[row][col+1];
                if(c == '.' || c == ' '|| c == 'o'){
                    col = col+1;
                }
            } catch (ArrayIndexOutOfBoundsException AIOOBE){
                c='x';
            }
        }
        if(direction==NORTH){
            try{
                c=board[row-1][col];
                if(c == '.' || c == ' '|| c == 'o'){
                    row = row-1;
                }
            } catch (ArrayIndexOutOfBoundsException AIOOBE){
                c='x';
            }
        }
        if(direction==SOUTH){
            try{
                c=board[row+1][col];
                if(c == '.' || c == ' '|| c == 'o'){
                    row = row+1;
                }
            } catch (ArrayIndexOutOfBoundsException AIOOBE){
                c='x';
            }
        }
        
        String observation = "\n\n"+observe();
       
        if(c=='x' && wall==false){
            GC.send("Suddenly","You stop, facing the wall. You should probably choose a direction."+observation);
            wall=true;
            return;
        }
        
        if(c==' '){
            GC.send("The maze continues", empty_hall.get(oRan.nextInt(empty_hall.size()))+observation);
            wall=false;
        }
        if(c=='.'){
            GC.send("The maze continues", dot_hall.get(oRan.nextInt(dot_hall.size()))+observation);
            for(UserData UD : GC.userList){
                EmojiDex.addEmoji("circle", UD);
            }
            ((PacChannel)GC).score += 1;
            board[row][col]=' ';
            wall=false;
        }
        if(c=='o'){
            GC.send("The maze continues", pow_hall.get(oRan.nextInt(pow_hall.size()))+observation);
            for(UserData UD : GC.userList){
                EmojiDex.addEmoji("circle", UD);
            }
            wall=false;
        }
        if(c=='A'){
            col = board[0].length-1;
        }
        if(c=='B'){
            col = 0;
        }
    }
    
    public void ghost(){
        
        char c = 'x';
        if(gdirection==WEST){
            try{
                c=board[grow][gcol-1];
                if(c == '.' || c == ' '|| c == 'o'){
                    gcol = gcol-1;
                }
            } catch (ArrayIndexOutOfBoundsException AIOOBE){
                c='x';
            }
        }
        if(gdirection==EAST){
            try{
                c=board[grow][gcol+1];
                if(c == '.' || c == ' '|| c == 'o'){
                    gcol = gcol+1;
                }
            } catch (ArrayIndexOutOfBoundsException AIOOBE){
                c='x';
            }
        }
        if(direction==NORTH){
            try{
                c=board[grow-1][gcol];
                if(c == '.' || c == ' '|| c == 'o'){
                    grow = row-1;
                }
            } catch (ArrayIndexOutOfBoundsException AIOOBE){
                c='x';
            }
        }
        if(direction==SOUTH){
            try{
                c=board[grow+1][gcol];
                if(c == '.' || c == ' '|| c == 'o'){
                    grow = grow+1;
                }
            } catch (ArrayIndexOutOfBoundsException AIOOBE){
                c='x';
            }
        }
        
        if(c=='x'){
            gdirection = oRan.nextInt(4);
        }
    }
    
    public String observe(){
        if(pow > 0){
            pow = pow - 1;
        }
        
        if(pow == 1){
            return "You feel yourself lose the power you gained before....";
        }
        
        if(oRan.nextInt(1000) < 30){
            for(int a = 0; a < board.length;++a){
                for(int b = 0; b < board[0].length;++b){
                    if(board[a][b] == '.'){
                        int row_dist = row - a;
                        int col_dist = col - b;
                        
                        if(direction == NORTH){
                            if(col_dist<0){
                                return "You can smell orbs to your right...";
                            } else{
                                return "You can smell orbs to your left...";
                            }
                        }
                        
                        if(direction == SOUTH){
                            if(row_dist<0){
                                return "You can smell orbs to your left...";
                            } else{
                                return "You can smell orbs to your right...";
                            }
                        }
                        
                        if(direction == EAST){
                            if(col_dist>0){
                                return "You can smell orbs to your left...";
                            } else{
                                return "You can smell orbs to your right...";
                            }
                        }
                        
                        if(direction == WEST){
                            if(col_dist<0){
                                return "You can smell orbs to your left...";
                            } else{
                                return "You can smell orbs to your right...";
                            }
                        }
                    }
                }
            }
        }
        
        
        if(direction == NORTH){
            try{
                
                if(board[row][col+1]=='.'){
                    return "You spot an orb to your right.";
                }
            } catch (Exception E){}
            try{
                if(board[row][col-1]=='.'){
                    return "You spot an orb to your left.";
                }
            } catch (Exception E){}
            try{    
                if(board[row][col+1]==' '){
                    return "An empty path lies to your right.";
                }
            } catch (Exception E){}
            try{
                if(board[row][col-1]==' '){
                    return "An empty path lies to your left.";
                }
            } catch (Exception E){}
        }
        
        if(direction == SOUTH){
            try{
                if(board[row][col+1]=='.'){
                    return "You spot an orb to your left.";
                }
            } catch (Exception E){}
            try{
                if(board[row][col-1]=='.'){
                    return "You spot an orb to your right.";
                }
            } catch (Exception E){}
            try{
                if(board[row][col+1]==' '){
                    return "An empty path lies to your left.";
                }
            } catch (Exception E){}
            try{
                if(board[row][col-1]==' '){
                    return "An empty path lies to your right.";
                }
            } catch (Exception E){}
        }
        
        if(direction == EAST){
            try{
                if(board[row+1][col]=='.'){
                    return "You spot an orb to your right.";
                }
            } catch (Exception E){}
            try{
                if(board[row-1][col]=='.'){
                    return "You spot an orb to your left.";
                }
            } catch (Exception E){}
            try{
                if(board[row-1][col]==' '){
                    return "An empty path lies to your left.";
                }
            } catch (Exception E){}
            try{
                if(board[row+1][col]==' '){
                    return "An empty path lies to your right.";
                }
            } catch (Exception E){}
        }
        
        if(direction == EAST){
            try{
                if(board[row-1][col]=='.'){
                    return "You spot an orb to your right.";
                }
            } catch (Exception E){}
            try{
                if(board[row+1][col]=='.'){
                    return "You spot an orb to your left.";
                }
            } catch (Exception E){}
            try{
                if(board[row+1][col]==' '){
                    return "An empty path lies to your left.";
                }
            } catch (Exception E){}
            try{
                if(board[row-1][col]==' '){
                    return "An empty path lies to your right.";
                }
            } catch (Exception E){}
        }
        
        
        int rdist = Math.abs(row - grow);
        int cdist = Math.abs(col - gcol);
        rdist = rdist * rdist;
        cdist = cdist * cdist;
        int dist = rdist + cdist;
        dist = (int)(Math.sqrt(dist));
        
        if(oRan.nextInt(100)<20 && (dist) < 5){
            return "An eerie presence can be felt...";
        }
        
        return "";
    }
}
