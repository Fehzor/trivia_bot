/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Trivia.Math;

import Bot.Fields.UserData;
import Bot.Launcher;
import Games.Trivia.Trivia;
import Games.Trivia.TriviaChannel;
import static Bot.SuperRandom.oRan;
import Games.EmojiDex;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;

/**
 *
 * @author FF6EB4
 */
public class MathTrivia extends Trivia{
    public IChannel mafChan = Launcher.client.getChannelByID(454800775592542220L);
    
    String equation = "2+2";
    String show = "2+2";
    
    public int numbers = 2;
    public int span = 10;
    public int signs = 1;
    public int reward = 1;
    public int totalSigns = 9;
    private ScriptEngine engine;
    
    long lastTime = 0L;
    
    public Sign q;
    public Sign z;
    public Sign or;
    public Sign qu;
    
    public MathTrivia(){
        ScriptEngineManager mgr = new ScriptEngineManager();
        this.engine = mgr.getEngineByName("JavaScript");
        
        z = new Sign();
        z.example = "12 Z 5 = 7";
        z.operation = "-";
        z.show = " Z ";
        
        q = new Sign();
        q.example = "3 Q 5 = -15";
        q.operation = "*-1*";
        q.show = " Q ";
        
        qu = new Sign();
        qu.example = " 5 ?11? 7 = 7; 11 ?11? 7 = 11";
        qu.operation = "== 11 ? 11 :";
        qu.show = " ?11? ";
        
        or = new Sign();
        or.example = "2 OR 2 = 2; 1 OR 2 = 3; 4 OR 8 = 12; 3 OR 5 = 7; ";
        or.operation = "|";
        or.show = " OR ";
        
    }

    public void generate(){
        equation = "";
        show = "";
        int div = 1;
        for(int i = 0; i < numbers; ++i){
            int num = 1 + oRan.nextInt(span / div);
            equation += ""+num;
            show += ""+num;
            
            if(i < numbers - 1){
                int sign = oRan.nextInt(signs);

                if(sign%totalSigns==0){
                    equation+="+";
                    show+=" + ";
                    div=1;
                } else if(sign%totalSigns==1){
                    equation+="-";
                    show+=" - ";
                    div=2;
                } else if(sign%totalSigns==2){
                    equation+="*";
                    show+=" x ";
                    div=5;
                } else if(sign%totalSigns==3){
                    equation+="/";
                    show+=" / ";
                    div=7;
                } else if(sign%totalSigns==4){
                    equation+="%";
                    show+=" REMAINDER ";
                    div=5;
                } else if(sign%totalSigns==5){
                    equation+=z.operation;
                    show+=z.show;
                    if(z.first){
                        z.first = false;
                        Launcher.send("Consider: "+z.example,mafChan);
                    }
                } else if(sign%totalSigns==6){
                    equation+=q.operation;
                    show+=q.show;
                    if(q.first){
                        q.first = false;
                        Launcher.send("Consider: "+q.example,mafChan);
                    }
                } else if(sign%totalSigns==7){
                    equation+=qu.operation;
                    show+=qu.show;
                    if(qu.first){
                        qu.first = false;
                        Launcher.send("Consider: "+qu.example,mafChan);
                    }
                } else if(sign%totalSigns==8){
                    equation+=or.operation;
                    show+=or.show;
                    if(or.first){
                        or.first = false;
                        Launcher.send("Consider: "+q.example,mafChan);
                    }
                }
            }
        }
    }

    public String question(){
        return show;
    }

    public boolean answer(String s){
        try{
            if(s.equals("jump")){
                if(mafChan.getMessageHistory().get(0).getAuthor().getLongID() == 144857966816788482L){
                    for(int i = 0; i < 100; ++i){
                        increaseDifficulty();
                    }
                    Launcher.send("Difficulty has been raised 100 times.",mafChan);
                }
            }
            
            if(Integer.parseInt(s) == (int)Double.parseDouble("" + engine.eval("1.0 * Math.round("+equation+")"))){
                increaseDifficulty();
                return true;
            } else {
                System.out.println((int)Double.parseDouble("" + engine.eval("1.0 * Math.round("+equation+")")));
            }
            
        } catch (Exception E){
            E.printStackTrace();
            return false;
        }
        return false;
    }
    
    public void increaseDifficulty(){
        if(System.currentTimeMillis() - lastTime > 1000*60*20){
            numbers = 2;
            span = 10;
            signs = 1;
            reward = 1;
            
            or.first = true;
            q.first = true;
            z.first = true;
            qu.first = true;
        }
        int what = oRan.nextInt(100);
        if(what == 0){
            numbers++;
            reward = (int)Math.floor(reward*1.3);
        } else if (what < 25){
            span++;
        } else if (what < 30){
            signs++;
            numbers = 2+oRan.nextInt(numbers);
            reward++;
            if(span > 20 && signs < totalSigns){
                span = 20;
            }
        } else if (what < 50){
            span+=3;
            reward++;
        } else {
            return;
        }
        lastTime = System.currentTimeMillis();
    }

    public void reward(IMessage mess, TriviaChannel TC){
        UserData UD = UserData.getUD(mess.getAuthor().getLongID());
        
        int num = this.rewardGet(TC.userList.size());
        if(oRan.nextInt(100) == 23){
            TC.send(mess.getAuthor().getName(),"+"+num+"  Math Emoji! Good fucking job! ");
        } else if(oRan.nextInt(100) == 23){
            TC.send(mess.getAuthor().getName(),"+"+num+" Math Emoji! Do you want a fucking cookie? ");
        } else if(oRan.nextInt(100) == 23){
            TC.send(mess.getAuthor().getName(),"+"+num+" Math Emoji! Grand fucking job.");
        } else if(oRan.nextInt(100) < 3){
            TC.send(mess.getAuthor().getName(),"+"+num+" Math Emoji!");
        }  else if(oRan.nextInt(1000) == 23){
            TC.send(mess.getAuthor().getName(),"+"+num+" Math Emoji!");
        } else if(oRan.nextInt(1000) == 23){
            TC.send(mess.getAuthor().getName(),"+"+num+" Math Emoji!");
        } else {
            TC.send(mess.getAuthor().getName(),"+"+num+" Math Emoji!");
        }
        
        for(int i = 0; i < num; ++i){
            EmojiDex.addEmoji("math", UD);
        }
    }

    private int rewardGet(int users){

        switch(users){
            case 0:
                return 1*reward;
            case 1:
                return 1*reward;
            case 2:
                return 1*reward;
            case 3:
                return 2*reward;
            case 4:
                return 3*reward;
            case 5:
                return 3*reward;
            case 6:
                return 4*reward;
            default:
                return 5*reward;
        }
    }
}
