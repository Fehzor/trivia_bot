/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Games.Trivia;

import Bot.Fields.UserData;
import Bot.Launcher;
import Games.GameChannel;
import static Bot.SuperRandom.oRan;
import java.awt.Color;
import java.util.ArrayList;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

/**
 *
 * @author FF6EB4
 */
public class TriviaChannel extends GameChannel{
    Trivia triv;
    
    public TriviaChannel(IChannel chan, Trivia triv){
        super(chan);
        this.triv = triv;
        triv.generate();
        send("What could it be???",triv.question());
    }
    
    public void tick(){
        super.tick();
        
        try{
            IMessage mess = chan.getMessageHistory().get(0);
            
            if(triv.answer(mess.getContent())){
                triv.reward(mess,this);
                
                triv.generate();
                send("What could it be???",triv.question());
            }
        } catch (Exception E){E.printStackTrace();}
    }
}
