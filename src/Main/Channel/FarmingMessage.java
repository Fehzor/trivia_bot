/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main.Channel;

import Main.Structure.Item;
import static Main.Structure.Stats.*;
import java.util.HashMap;

/**
 *
 * @author FF6EB4
 */
public class FarmingMessage {
    public String text = "You farmed.....";
    public long LifeForceEXP = 0L;
    public long PhysEXP = 0L;
    public long EndureEXP = 0L;
    public long EmpEXP = 0L;
    public long LuckEXP = 0L;
    public long LibidoEXP = 0L;
    
    public HashMap<Long,Long> items = new HashMap<>();
    
    public FarmingMessage(){}
    
    public void put(String item, long num){
        Item I = Item.getItem(item);
        items.put(I.ID, items.get(I.ID)+num);
    }
    
    public void setEXP(int stat, long exp){
        if(stat==LIFE_FORCE){
            this.LifeForceEXP+=exp;
        }
        if(stat==PHYSICAL){
            this.PhysEXP+=exp;
        }
        if(stat==ENDURANCE){
            this.EndureEXP+=exp;
        }
        if(stat==EMPATHY){
            this.EmpEXP+=exp;
        }
        if(stat==LUCK){
            this.LuckEXP+=exp;
        }
        if(stat==LIBIDO){
            this.LibidoEXP+=exp;
        }
    }
    
    public void add(FarmingMessage FM){
        this.LifeForceEXP += FM.LifeForceEXP;
        this.PhysEXP += FM.PhysEXP;
        this.EndureEXP += FM.EndureEXP;
        this.EmpEXP += FM.EmpEXP;
        this.LuckEXP += FM.LuckEXP;
        this.LibidoEXP += FM.LibidoEXP;
        
        for(long l : FM.items.keySet()){
            Item get = Item.getItem(l);
            this.put(get.name.getData(),FM.items.get(get));
        }
    }
    
    public String getmessage(){
        String ret=text+"\n\n";
        
        if(LifeForceEXP > 0){
            ret+="+"+LifeForceEXP+" life force exp\n";
        }
        if(PhysEXP > 0){
            ret+="+"+PhysEXP+" physical exp\n";
        }
        if(EndureEXP > 0){
            ret+="+"+EndureEXP+" endurance exp\n";
        }
        if(EmpEXP > 0){
            ret+="+"+EmpEXP+" empathy exp\n";
        }
        if(LuckEXP > 0){
            ret+="+"+LuckEXP+" luck exp\n";
        }
        if(LibidoEXP > 0){
            ret+="+"+LibidoEXP+" libido exp\n";
        }
        
        for(long l : items.keySet()){
            Item I = Item.getItem(l);
            ret+="+"+items.get(l)+" "+I.name.getData()+"\n";
        }
        
        return ret;
    }
    
}
