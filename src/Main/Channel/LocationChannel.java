/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main.Channel;

import Bot.Fields.Field;
import Games.*;
import Bot.Fields.UserData;
import Bot.Launcher;
import Bot.Settings;
import static Bot.SuperRandom.oRan;
import Main.Structure.StateMachine;
import Main.Structure.Feature;
import Main.Structure.Item;
import Main.Structure.TaskManager;
import com.vdurmont.emoji.EmojiManager;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import org.json.JSONObject;
import sx.blah.discord.handle.obj.ICategory;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RateLimitException;

/**
 *
 * @author FF6EB4
 */
public class LocationChannel extends Thread{
    
    public static final long TIME_OUT = 1000 * 60 * 3; // 3 minutes
    public Field<Color> col;
    
    public IChannel chan;
    public IRole role;
    
    public ArrayList<UserData> activeUsers = new ArrayList<>();
    public ArrayList<UserData> passiveUsers = new ArrayList<>();
    public HashMap<UserData,FarmingMessage> farmMessages = new HashMap<>();
    
    public static Field<ArrayList<Long>> IDs;
    public static Field<HashMap<Long,Long>> roleIDs;
    
    public Field<ArrayList<Long>> featIDs;
    
    public Field<ArrayList<Long>> connections;
    
    public static HashMap<Long,LocationChannel> mappings = new HashMap<>();
    public static HashMap<String,LocationChannel> nameMappings = new HashMap<>();
    
    
    public Field<Long> SM;
    
    public LocationChannel(IChannel chan, IRole role){
        this.chan = chan;
        this.role = role;
        col = new Field("LC_"+this.chan.getLongID(),"COLOR",Color.LIGHT_GRAY);
        
        this.connections = new Field<>("LC_"+this.chan.getLongID(),"CONNECTIONS",new ArrayList<>());
        
        featIDs = new Field<>("LC_"+chan.getLongID(),"FEATS",new ArrayList<>());
        
        mappings.put(chan.getLongID(),this);
        nameMappings.put(chan.getName(),this);
        
        SM = new Field<>("LC_"+this.chan.getLongID(),"SM",-1L);
        this.start();
    }
    
    public LocationChannel(ICategory cat, String name, Color C){
        if(IDs == null){
            IDs = new Field<>("LC","IDS",new ArrayList<>());
        }
        
        
        this.chan = cat.createChannel(name);
        role = Launcher.client.getGuilds().get(0).createRole();
        col = new Field("LC_"+this.chan.getLongID(),"COLOR",C);
        col.writeData(C);
        
        this.connections = new Field<>("LC_"+this.chan.getLongID(),"CONNECTIONS",new ArrayList<>());
        
        
        IRole everyone = null; 
        for(IRole I : Launcher.client.getRoles()){
            if(I.isEveryoneRole()){
                everyone = I;
            }
        }
        EnumSet<Permissions> toAdd = EnumSet.of(Permissions.READ_MESSAGES, Permissions.SEND_MESSAGES);
        EnumSet<Permissions> toRemove = EnumSet.of(Permissions.MANAGE_CHANNEL);
        EnumSet<Permissions> none = EnumSet.of(Permissions.CHANGE_NICKNAME);
        
        role.edit(everyone.getColor(), false, name, none, true);
        chan.overrideRolePermissions(role,toAdd,toRemove);
        if(!chan.getName().equals("wayward_beach")){
            chan.overrideRolePermissions(everyone,none,toAdd);
        }
        
        
        IDs.getData().add(chan.getLongID());
        IDs.write();
        roleIDs.getData().put(chan.getLongID(), role.getLongID());
        roleIDs.write();
        
        featIDs = new Field<>("LC_"+chan.getLongID(),"FEATS",new ArrayList<>());
        featIDs.write();
        
        mappings.put(chan.getLongID(),this);
        nameMappings.put(chan.getName(),this);
        
        SM = new Field<>("LC_"+this.chan.getLongID(),"SM",-1L);
        this.start();
    }
    
    public void run(){
        while(true){
            try{
                Thread.sleep(30*1000);
                inactivity();
                
            } catch (Exception E){}
        }
    }
    
    public static void loadChannels(){
        IDs = new Field<>("LC","IDS",new ArrayList<>());
        roleIDs = new Field<>("LC","RIDS",new HashMap<>());
        for(long L : IDs.getData()){
            IChannel cha = Launcher.client.getChannelByID(L);
            
            IRole rol = null;
            for(IRole R : Launcher.client.getRoles()){
                if(R.getName().equals(cha.getName())){
                    rol = R;
                }
            }
            
            LocationChannel make = new LocationChannel(cha, rol);
        }
    }
    

    
    public void connect(IChannel IC){
        if(this.connections.getData().contains(IC.getLongID())){
            return;
        }
        
        this.connections.getData().add(IC.getLongID());
        this.connections.write();
    }
    
    public static LocationChannel getChannel(IChannel get){
        if(mappings.containsKey(get.getLongID())){
            return mappings.get(get.getLongID());
        } else {
            return null;
        }
    }
    
    public void tick(IMessage mess){
        inactivity();
        boolean delete = true;
        
        if(Settings.quiet.getData()){
            if(mess.getContent().toLowerCase().contains("say")){
                delete = false;
            }
        } else {
            delete = false;
        }
        
        UserData UD = UserData.getUD(mess.getAuthor());
        
        for(long L : featIDs.getData()){
            Feature f = Feature.getFeat(L);
            
            if(f.name.getData().toLowerCase().equals(mess.getContent().toLowerCase())){
                delete = true;
                
                String result = f.act(this,UD);
                this.displayFeature(mess.getAuthor().getName(),f,result);
            }
        }
        
        
        for(long L : UD.inventory.getData().keySet()){
            Item I = Item.getItem(L);
            
            if(I.name.getData().toLowerCase().equals(mess.getContent().toLowerCase())){
                delete = true;
                
                String result = I.active(this,UD);
                this.displayItem(mess.getAuthor().getName(),I,result);
            }
        }
        
        if(SM.getData() != -1){
            StateMachine SMget = StateMachine.SMs.get(SM.getData());
            boolean action = SMget.playerMove(this, UD, mess.getContent().toLowerCase());
            
            if(SM.getData() != -1 && action){
                SMget.enemyMove(this, UD);
            }
        }
        
        if(delete){
            //mess.delete();
        }
    }
    
    public void sendFarmingMessage(UserData UD, FarmingMessage FM){
        if(this.farmMessages.containsKey(UD)){
            this.farmMessages.get(UD).add(FM);
        } else {
            this.farmMessages.put(UD,FM);
        }
    }
    
    public void send(String title,String desc){
       
       try{
           //System.out.println("BUILDER");
           EmbedBuilder builder = new EmbedBuilder();


           //System.out.println("COLOR 2");
           builder.withColor(col.getData().getRed(), col.getData().getGreen(), col.getData().getBlue());

           //System.out.println("DESCRIPTION");           
           builder.withTitle(title);

           builder.appendDescription(desc);

           //System.out.println("SENDING");
           Launcher.send(builder, chan);
       } catch(Exception E){E.printStackTrace();}
   }
    
    public void displayFeatures(){
       
       try{
           //System.out.println("BUILDER");
           EmbedBuilder builder = new EmbedBuilder();


           //System.out.println("COLOR 2");
           builder.withColor(col.getData().getRed(), col.getData().getGreen(), col.getData().getBlue());

           //System.out.println("DESCRIPTION");           
           builder.withTitle("INTERACTABLE FEATURES AT "+chan.getName().toUpperCase()+":");

           for(long l : featIDs.getData()){
               Feature F = Feature.getFeat(l);
               String desc = "\n"+F.emoji.getData() +" **"+ F.name.getData() +"**\n"
                       +"*"+ F.desc.getData() + "*\n";
               
               builder.appendDescription(desc);
           }
           
           //System.out.println("SENDING");
           Launcher.send(builder, chan);
       } catch(Exception E){E.printStackTrace();}
   }
    
    public void displayFeature(String requester,Feature F,String result){
       try{
           //System.out.println("BUILDER");
           EmbedBuilder builder = new EmbedBuilder();


           //System.out.println("COLOR 2");
           builder.withColor(F.color.getData().getRed(), F.color.getData().getGreen(), F.color.getData().getBlue());

           //System.out.println("DESCRIPTION");           
           builder.withTitle(requester);

           builder.appendDescription(F.emoji.getData()+" **"+F.name.getData().toUpperCase()+"**\n"+F.desc.getData());
           builder.appendDescription("\n\n");
           builder.appendDescription(result);
            //System.out.println("SENDING");
           Launcher.send(builder, chan);
       } catch(Exception E){E.printStackTrace();}
   }
    
    public void displayItem(String requester,Item I,String result){
       try{
           //System.out.println("BUILDER");
           EmbedBuilder builder = new EmbedBuilder();


           //System.out.println("COLOR 2");
           builder.withColor(I.color.getData().getRed(), I.color.getData().getGreen(), I.color.getData().getBlue());

           //System.out.println("DESCRIPTION");           
           builder.withTitle(requester);

           builder.appendDescription(I.emoji.getData()+" **"+I.name.getData().toUpperCase()+"**\n"+I.desc.getData());
           builder.appendDescription("\n\n");
           builder.appendDescription(result);
            //System.out.println("SENDING");
           Launcher.send(builder, chan);
       } catch(Exception E){E.printStackTrace();}
   }
    
    //Checks for inactivity
    public void inactivity(){
        long now = System.currentTimeMillis();
        
        for(int i = 0; i < activeUsers.size(); ++i){
            if(now - activeUsers.get(activeUsers.size()-1).lastMessage.getData() > TIME_OUT){
                passiveUsers.add(activeUsers.remove(activeUsers.size()-1));
            } else {
                try{
                UserData UD = activeUsers.get(activeUsers.size()-1);
                send(UD.name,this.farmMessages.get(UD).getmessage());
                this.farmMessages.remove(UD);
                } catch (Exception E){}
            }
        }
    }
    
    public  boolean addUser(UserData UD){
        boolean ret = true;
        if(this.activeUsers.contains(UD)){
            ret = false;
            
            IUser user = Launcher.client.getUserByID(Long.parseLong(UD.ID));
            
            //user.addRole(role);
            for(long L : connections.getData()){
                LocationChannel LC = LocationChannel.mappings.get(L);
                if(!user.hasRole(LC.role)){
                    user.addRole(LC.role);
                    UD.areas.getData().add(chan.getName());
                    UD.areas.write();
                }
            }
        } else {
            if(this.passiveUsers.contains(UD)){
                passiveUsers.remove(UD);
                ret = false;
                try{
                send(UD.name,this.farmMessages.get(UD).getmessage());
                this.farmMessages.remove(UD);
                } catch (Exception E){}
            }
            //this.send(UD.name,"...has arrived! Say hello!");
            
            this.activeUsers.add(UD);
            IUser user = Launcher.client.getUserByID(Long.parseLong(UD.ID));
            
            boolean rate = true;
            while(rate){
                try{
                user.addRole(role);
                UD.areas.getData().add(chan.getName());
                UD.areas.write();
                rate = false;
                } catch (RateLimitException RLE){
                    rate = true;
                }
            }
            
            for(long L : connections.getData()){
                LocationChannel LC = LocationChannel.mappings.get(L);
                rate = true;
                while(rate){
                    try{
                    user.addRole(LC.role);
                    UD.areas.getData().add(chan.getName());
                    UD.areas.write();
                    rate = false;
                    } catch (RateLimitException RLE){
                        rate = true;
                    }
                }
            }
            
            
            this.displayFeatures();
        }
        
        return ret;
    }
    
    public void removeUser(UserData UD, LocationChannel next){
        if(this.passiveUsers.contains(UD)){
            passiveUsers.remove(UD);
        }
        if(this.activeUsers.contains(UD)){
            activeUsers.remove(UD);
        }
        IUser user = Launcher.client.getUserByID(Long.parseLong(UD.ID));
        user.removeRole(role);
        for(long L : connections.getData()){
            LocationChannel LC = LocationChannel.mappings.get(L);
            if(!next.connections.getData().contains(L) && next.chan.getLongID() != L){
                user.removeRole(LC.role);
            }
        }
        if(TaskManager.times.getData().containsKey(Long.parseLong(UD.ID))){
            TaskManager.times.getData().remove(Long.parseLong(UD.ID));
            TaskManager.times.write();
        }
    }
    
    public static void deleteAll(){
        for(long ID : IDs.getData()){
            try{
                IChannel IC = Launcher.client.getChannelByID(ID);
                IRole IR = Launcher.client.getRoleByID(roleIDs.getData().get(ID));

                IC.delete();
                IR.delete();
            } catch (Exception E){}
        }
        
        mappings = new HashMap<>();
        roleIDs.writeData(new HashMap<>());
        IDs.writeData(new ArrayList<>());
    }
    
    public static void loadFromFile(){
        
        TaskManager.deleteAll();
        
        Item.deleteAll();
        Item.loadFromFile();
        StateMachine.deleteAll();
        StateMachine.loadFromFile();
        Feature.deleteAll();
        Feature.loadFromFile();
        deleteAll();
        
        JSONObject json;
        File F = new File("resources/locationChannels.json");
        try{
            Scanner oScan = new Scanner(F);

            String get = "";

            while(oScan.hasNextLine()){
                get+=oScan.nextLine()+"\n";
            }

            json = new JSONObject(get);

            Iterator<?> keys = json.keys();

            
            HashMap<LocationChannel,ArrayList<String>> cones = new HashMap<>();
            while( keys.hasNext() ) {
                String key = (String)keys.next();
                JSONObject entry = (JSONObject)json.get(key);
                
                ICategory cat = Launcher.client.getCategoryByID(entry.getLong("category"));
                
                
                long l = Long.parseLong(entry.getString("color"), 16);
                Color C = new Color((int)l);
                
                LocationChannel LC = new LocationChannel(cat,key, C);
                
                
                
                cones.put(LC,new ArrayList<>());
                for (Object o : entry.getJSONArray("connections")){
                    String s = (String)o;
                    cones.get(LC).add(s);
                }
                
                for (Object o : entry.getJSONArray("features")){
                    String s = (String)o;
                    LC.featIDs.getData().add(Feature.getFeat(s).ID);
                    LC.featIDs.write();
                }
            }
            
            for(LocationChannel LC : cones.keySet()){
                for(String S : cones.get(LC)){
                    LocationChannel add = nameMappings.get(S);
                    LC.connect(add.chan);
                }
            }
        } catch (Exception E){E.printStackTrace();}
    }
}
