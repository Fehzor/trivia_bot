/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main.Structure;

import Main.Channel.LocationChannel;
import Bot.Fields.Field;
import Bot.Fields.UserData;
import Bot.Launcher;
import static Main.Channel.LocationChannel.nameMappings;
import com.vdurmont.emoji.EmojiManager;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import org.json.JSONObject;
import sx.blah.discord.handle.obj.ICategory;
import sx.blah.discord.handle.obj.IMessage;

/**
 *
 * @author FF6EB4
 */
public class Feature {
    private static Field<Long>nextID;
    public long ID;
    
    public Field<String> name;
    public Field<String> desc;
    public Field<String> emoji;
    public Field<Color> color;
    
    public Field<String> functionality;
    
    private static HashMap<Long,Feature> feats = new HashMap<>();
    private static Field<HashMap<String, Long>> featsByName;
    
    public Feature(JSONObject stuff){
        if(nextID == null){
            nextID = new Field("FEATURE","ID",0L);
        }
        if(featsByName == null){
            featsByName = new Field<>("FEAT","BYNAME",new HashMap<>());
        }
        ID = nextID.getData();
        nextID.writeData(nextID.getData()+1);
        
        name = new Field<>("FEAT_"+ID,"NAME",(String)stuff.get("name"));
        name.writeData((String)stuff.get("name"));
        desc = new Field<>("FEAT_"+ID,"DESC",(String)stuff.get("desc"));
        desc.writeData(stuff.getString("desc"));
        emoji = new Field<>("FEAT_"+ID,"EMOJI",EmojiManager.getForAlias((String)stuff.get("emoji")).getUnicode());
        emoji.writeData(EmojiManager.getForAlias((String)stuff.get("emoji")).getUnicode());
        color = new Field<>("FEAT_"+ID,"COLOR",new Color((int)stuff.get("red"),(int)stuff.get("green"),(int)stuff.get("blue")));
        color.writeData(new Color((int)stuff.get("red"),(int)stuff.get("green"),(int)stuff.get("blue")));
        
        functionality = new Field<>("FEAT_"+ID,"FUNC",((JSONObject)stuff.get("function")).toString());
        functionality.writeData(((JSONObject)stuff.get("function")).toString());
        
        feats.put(this.ID,this);
        featsByName.getData().put(name.getData(),ID);
        featsByName.write();
    }
    
    public Feature(long ID){
        if(featsByName == null){
            featsByName = new Field<>("FEAT","BYNAME",new HashMap<>());
        }
        this.ID = ID;
        name = new Field<>("FEAT_"+ID,"NAME","NAMELESS OBJECT OF LEGEND");
        desc = new Field<>("FEAT_"+ID,"DESC","IT HAS NO NAME");
        emoji = new Field<>("FEAT_"+ID,"EMOJI","cowboy");
        color = new Field<>("FEAT_"+ID,"COLOR",Color.DARK_GRAY);
        
        functionality = new Field<>("FEAT_"+ID,"FUNC","");
        
        feats.put(this.ID,this);
        featsByName.getData().put(name.getData(),ID);
        featsByName.write();
    }
    
    public static Feature getFeat(long ID){
        if(feats.containsKey(ID)){
            return feats.get(ID);
        }
        return new Feature(ID);
    }
    
    public static Feature getFeat(String name){
        if(featsByName == null){
            featsByName = new Field<>("FEAT","BYNAME",new HashMap<>());
        }
        return getFeat(featsByName.getData().get(name));
    }
    
    public String act(LocationChannel LC, UserData UD){
        String get = functionality.getData();
        JSONObject JSON = new JSONObject(get);
        
        String result = FunctionParser.parse(JSON, LC, UD);
        return result;
        //LC.send(UD.name, result);
    }
    
    public static void deleteAll(){
        if(nextID == null){
            nextID = new Field("FEATURE","ID",0L);
        }
        
        if(featsByName == null){
            featsByName = new Field<>("FEAT","BYNAME",new HashMap<>());
        }
        featsByName.writeData(new HashMap<>());
        feats = new HashMap<>();
        nextID.writeData(0L);
    }
    
    public static void loadFromFile(){
        JSONObject json;
        File F = new File("resources/features.json");
        try{
            Scanner oScan = new Scanner(F);

            String get = "";

            while(oScan.hasNextLine()){
                get+=oScan.nextLine()+"\n";
            }

            json = new JSONObject(get);

            Iterator<?> keys = json.keys();

            
            HashMap<LocationChannel,ArrayList<String>> cones = new HashMap<>();
            while( keys.hasNext() ) {
                String key = (String)keys.next();
                JSONObject entry = (JSONObject)json.get(key);
                
                Feature Featurrr = new Feature(entry);
            }

        } catch (Exception E){E.printStackTrace();}
    }
}
