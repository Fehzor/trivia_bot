/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main.Structure;

import Main.Channel.LocationChannel;
import Bot.Fields.UserData;
import Bot.Launcher;
import Main.Channel.FarmingMessage;
import Main.Structure.StateMachine;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONObject;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

/**
 *
 * @author FF6EB4
 */
public class FunctionParser {
    
    public static final long DEFAULT_TIME = 75*1000; // One minute 15 seconds
    
    public static String parse(JSONObject json, LocationChannel LC, UserData UD){
        String com = json.getString("code");
        if(com.equals("role")){
            return role(json,LC,UD);
        }
        if(com.equals("and")){
            return and(json,LC,UD);
        }
        if(com.equals("give")){
            return give(json,LC,UD);
        }
        if(com.equals("farm")){
            return farm(json,LC,UD);
        }
        if(com.equals("check")){
            return check(json,LC,UD);
        }
        if(com.equals("damage")){
            return takeDamage(json,LC,UD);
        }
        if(com.equals("deal_damage")){
            return dealDamage(json,LC,UD);
        }
        if(com.equals("state")){
            return state(json,LC,UD);
        }
        if(com.equals("set_machine")){
            return setMachine(json,LC,UD);
        }
        if(com.equals("end_combat")){
            return endCombat(json,LC,UD);
        }
        if(com.equals("none")){
            return json.getString("text");
        }
        if(com.equals("consume")){
            return consume(json,LC,UD);
        }
        if(com.equals("equip")){
            return equip(json,LC,UD);
        }
        
        return "";
    }
    
    public static String role(JSONObject json, LocationChannel LC, UserData UD){
        String name = json.getString("name");
        IRole rol = Launcher.client.getGuilds().get(0).getRolesByName(name).get(0);
        IUser usr = Launcher.client.getUserByID(Long.parseLong(UD.ID));
        
        usr.addRole(rol);
        
        return "Attaained role "+rol.getName()+"!";
    }
    
    public static String and(JSONObject json, LocationChannel LC, UserData UD){
        String ret = "";
        
        JSONObject json1 = json.getJSONObject("one");
        JSONObject json2 = json.getJSONObject("two");
        
        ret += parse(json1,LC,UD) + "\n\n" + parse(json2,LC,UD);
        
        return ret;
    }
    
    public static String give(JSONObject json, LocationChannel LC, UserData UD){
        String itemString = json.getString("item");

        Item I = Item.getItem(itemString);

        UD.giveItem(I, 1);

        return json.getString("text");
    }
    
    public static String farm(JSONObject json, LocationChannel LC, UserData UD){
        String function = ((JSONObject)json.get("action")).toString();
            
        TaskManager.add(Long.parseLong(UD.ID), LC.chan.getLongID(), DEFAULT_TIME, function,true);

        return json.getString("text");
    }
    
    public static String check(JSONObject json, LocationChannel LC, UserData UD){
        int stat = Stats.getStatNumber(json.getString("exp"));
        int rLevel = json.getInt("level");
        int pLevel = Stats.getLevel(UD,stat);

        boolean success = Stats.success(pLevel,rLevel);

        int exp = Stats.rewardEXP(pLevel, rLevel, (int)(DEFAULT_TIME/1000L), success);

        Stats.giveEXPtoUD(UD, exp, stat);
        if(success){
            JSONObject next = json.getJSONObject("action");
            String text = parse(next,LC,UD);
            
            
            FarmingMessage FM = new FarmingMessage();
            FM.setEXP(stat,exp);
            FM.text=text;
            LC.sendFarmingMessage(UD, FM);
            
            return json.getString("text");
        }
        
        FarmingMessage FM = new FarmingMessage();
        FM.setEXP(stat,exp);
        LC.sendFarmingMessage(UD, FM);
        
        return json.getString("fail_text");
    }
    
    public static String takeDamage(JSONObject json, LocationChannel LC, UserData UD){
        UD.LifeForce.writeData(UD.LifeForce.getData() - json.getLong("amount"));
        UD.LifeForce.write();
        return json.getString("text")+"\n"+"You took "+json.getLong("amount")+" damage!";
    }
    
    public static String dealDamage(JSONObject json, LocationChannel LC, UserData UD){
        StateMachine SMget = StateMachine.SMs.get(LC.SM.getData());
        SMget.lifeForce.writeData(SMget.lifeForce.getData() - (long)json.getLong("amount"));
        if(SMget.lifeForce.getData() <= 0){
            JSONObject reward = new JSONObject(SMget.reward.getData());
            Iterator<?> keys = reward.keys();
            while( keys.hasNext() ) {
                String key = (String)keys.next();
                //System.out.println(key);
                Item I = Item.getItem(key);

                for(Long ID : SMget.playerMap.getData().keySet()){
                    UserData UsDa = UserData.getUD(ID);
                    UsDa.giveItem(I, reward.getInt(key));
                }
            }

            SMget.lifeForce.writeData(-1L);
            LC.SM.writeData(-1L);

            return "The mighty "+SMget.name.getData()+" has fallen! All have been rewarded!";
        }
        return json.getString("text")+"\n"+"You dealt "+json.getLong("amount")+" damage!";
    }
    
    public static String state(JSONObject json, LocationChannel LC, UserData UD){
        StateMachine SMget = StateMachine.SMs.get(LC.SM.getData());
        SMget.playerMap.getData().put(Long.parseLong(UD.ID), json.getString("state"));
        return json.getString("text");
    }
    
    public static String setMachine(JSONObject json, LocationChannel LC, UserData UD){
        if(LC.SM.getData() == -1){
            StateMachine SMget = StateMachine.get(json.getString("machine"));
            LC.SM.writeData(SMget.ID);

            SMget.registerPlayer(UD);

            return SMget.name.getData() + " appears!";
        } else {
            StateMachine SMget = StateMachine.SMs.get(LC.SM.getData());
            SMget.registerPlayer(UD);
            return UD.name+" has joined the fight!";
        }
    }
    
    public static String endCombat(JSONObject json, LocationChannel LC, UserData UD){
        LC.SM.writeData(-1L);
        return json.getString("text");
    }
    
    public static String consume(JSONObject json, LocationChannel LC, UserData UD){
        String nam = json.getString("name");
        Item I = Item.getItem(nam);
        if(UD.inventory.getData().containsKey(I.ID) && UD.inventory.getData().get(I.ID) > 0){
            UD.giveItem(I, -1L);
            I.consume(LC, UD);

            return json.getString("consume_text");
        }
        return "";
    }
    
    public static String equip(JSONObject json, LocationChannel LC, UserData UD){
     try{
        Item I = Item.getItem(WordUtils.capitalizeFully(json.getString("item")));
        
        
        if(UD.inventory.getData().containsKey(I.ID)&& UD.inventory.getData().get(I.ID)>0){
            UD.giveItem(Item.getItem(UD.weapon.getData()), 1);
            UD.weapon.writeData(I.ID);
            UD.giveItem(I, -1);
            
            return "Equipped "+I.name.getData();
        }
        
        } catch (Exception E){
            E.printStackTrace();
        }
     
     return "Didn't equip anything...";
    }
    
}
