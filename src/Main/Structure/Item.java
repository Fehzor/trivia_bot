/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main.Structure;

import Main.Structure.FunctionParser;
import Main.Channel.LocationChannel;
import Bot.Fields.Field;
import Bot.Fields.UserData;
import Bot.Launcher;
import Main.Channel.FarmingMessage;
import Main.Structure.StateMachine;
import static Main.Channel.LocationChannel.nameMappings;
import static Main.Structure.Stats.*;
import com.vdurmont.emoji.EmojiManager;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import org.json.JSONObject;
import sx.blah.discord.handle.obj.ICategory;
import sx.blah.discord.handle.obj.IMessage;

/**
 *
 * @author FF6EB4
 */
public class Item {
    private static Field<Long>maxID;
    public long ID;
    
    public Field<String> name;
    public Field<String> desc;
    public Field<String> emoji;
    public Field<Color> color;
    
    public Field<String> functionality;
    public Field<String> combat;
    
    public Field<String> consumeText;
    
    private static HashMap<Long,Item> items = new HashMap<>();
    private static Field<HashMap<String, Long>> defaultItems;
    
    public Field<Long> LifeForceEXP;
    public Field<Long> LifeForce;
    
    public Field<Long> PhysEXP;
    public Field<Long> EndureEXP;
    public Field<Long> EmpEXP;
    public Field<Long> LuckEXP;
    public Field<Long> LibidoEXP;
    
    public Item(JSONObject stuff){
        if(defaultItems == null){
            defaultItems = new Field<>("ITEM","DEFAULT",new HashMap<>());
        }
        if(maxID == null){
            maxID = new Field<>("ITEM","ID",0L);
        }
        
        ID = stuff.getLong("ID");
        
        name = new Field<>("ITEM_"+ID,"NAME",(String)stuff.get("name"));
        desc = new Field<>("ITEM_"+ID,"DESC",(String)stuff.get("desc"));
        emoji = new Field<>("ITEM_"+ID,"EMOJI","->");
        color = new Field<>("ITEM_"+ID,"COLOR",new Color((int)stuff.get("red"),(int)stuff.get("green"),(int)stuff.get("blue")));
        
        name.writeData(stuff.getString("name"));
        desc.writeData(stuff.getString("desc"));
        
        try{
        emoji.writeData(EmojiManager.getForAlias(stuff.getString("emoji")).getUnicode());
        } catch (Exception E){}
        
        color.writeData(new Color((int)stuff.get("red"),(int)stuff.get("green"),(int)stuff.get("blue")));
        
        functionality = new Field<>("ITEM_"+ID,"FUNC",((JSONObject)stuff.get("function")).toString());
        functionality.writeData(((JSONObject)stuff.get("function")).toString());
        
        combat = new Field<>("ITEM_"+ID,"COMBAT",((JSONObject)stuff.get("combat")).toString());
        combat.writeData(((JSONObject)stuff.get("combat")).toString());
        
        LifeForceEXP = new Field<>("ITEM_"+ID,"LFEXP",stuff.getLong("LifeForceEXP"));
        LifeForce = new Field<>("ITEM_"+ID,"LF",stuff.getLong("LifeForce"));
        
        LifeForceEXP.writeData(stuff.getLong("LifeForceEXP"));
        LifeForce.writeData(stuff.getLong("LifeForce"));
        
        PhysEXP = new Field<>("ITEM_"+ID,"PHEXP",stuff.getLong("PhysEXP"));
        EndureEXP = new Field<>("ITEM_"+ID,"ENDXP",stuff.getLong("EndureEXP"));
        EmpEXP = new Field<>("ITEM_"+ID,"EMPEXP",stuff.getLong("EmpEXP"));
        LuckEXP = new Field<>("ITEM_"+ID,"LUKEXP",stuff.getLong("LuckEXP"));
        LibidoEXP = new Field<>("ITEM_"+ID,"LIBEXP",stuff.getLong("LibidoEXP"));
        
        consumeText = new Field<>("ITEM_"+ID,"CONTEXT",stuff.getString("consume_text"));
        
        PhysEXP.writeData(stuff.getLong("PhysEXP"));
        EndureEXP.writeData(stuff.getLong("EndureEXP"));
        EmpEXP.writeData(stuff.getLong("EmpEXP"));
        LuckEXP.writeData(stuff.getLong("LuckEXP"));
        LibidoEXP.writeData(stuff.getLong("LibidoEXP"));
        
        consumeText.writeData(stuff.getString("consume_text"));
        
        if(ID > maxID.getData()){
            maxID.writeData(ID);
        }
        
        items.put(this.ID,this);
        defaultItems.getData().put(name.getData(),ID);
        defaultItems.write();
    }
    
    public Item(long ID){
        if(defaultItems == null){
            defaultItems = new Field<>("ITEM","DEFAULT",new HashMap<>());
        }
        this.ID = ID;
        name = new Field<>("ITEM_"+ID,"NAME","NAMELESS OBJECT OF LEGEND");
        desc = new Field<>("ITEM_"+ID,"DESC","IT HAS NO NAME");
        emoji = new Field<>("ITEM_"+ID,"EMOJI","->");
        color = new Field<>("ITEM_"+ID,"COLOR",Color.DARK_GRAY);
        
        functionality = new Field<>("ITEM_"+ID,"FUNC","");
        
        combat = new Field<>("ITEM_"+ID,"COMBAT","");
        
        LifeForceEXP = new Field<>("ITEM_"+ID,"LFEXP",0L);
        LifeForce = new Field<>("ITEM_"+ID,"LF",0L);
        
        PhysEXP = new Field<>("ITEM_"+ID,"PHEXP",0L);
        EndureEXP = new Field<>("ITEM_"+ID,"ENDXP",0L);
        EmpEXP = new Field<>("ITEM_"+ID,"EMPEXP",0L);
        LuckEXP = new Field<>("ITEM_"+ID,"LUKEXP",0L);
        LibidoEXP = new Field<>("ITEM_"+ID,"LIBEXP",0L);
        
        items.put(this.ID,this);
        defaultItems.getData().put(name.getData(),ID);
        defaultItems.write();
    }
    
    public static Item getItem(long ID){
        if(items.containsKey(ID)){
            return items.get(ID);
        }
        return new Item(ID);
    }
    
    public static Item getItem(String name){
        if(defaultItems == null){
            defaultItems = new Field<>("ITEM","DEFAULT",new HashMap<>());
        }
        return getItem(defaultItems.getData().get(name));
    }
    
    public void consume(LocationChannel LC, UserData UD){
        UD.LifeForce.writeData(UD.LifeForce.getData()+this.LifeForce.getData());
        UD.LifeForceEXP.writeData(UD.LifeForceEXP.getData()+this.LifeForceEXP.getData());
        
        UD.PhysEXP.writeData(UD.PhysEXP.getData()+this.PhysEXP.getData());
        UD.EmpEXP.writeData(UD.EmpEXP.getData()+this.EmpEXP.getData());
        UD.EndureEXP.writeData(UD.EndureEXP.getData()+this.EndureEXP.getData());
        UD.LuckEXP.writeData(UD.LuckEXP.getData()+this.LuckEXP.getData());
        UD.LibidoEXP.writeData(UD.LibidoEXP.getData()+this.LibidoEXP.getData());
        
        FarmingMessage FM = new FarmingMessage();
        FM.setEXP(LIFE_FORCE, this.LifeForceEXP.getData());
        FM.setEXP(PHYSICAL, this.PhysEXP.getData());
        FM.setEXP(EMPATHY, this.EmpEXP.getData());
        FM.setEXP(ENDURANCE, this.EndureEXP.getData());
        FM.setEXP(LUCK, this.LuckEXP.getData());
        FM.setEXP(LIBIDO, this.LibidoEXP.getData());
        
        FM.text = this.consumeText.getData();
        
        LC.sendFarmingMessage(UD, FM);
        
    }
    
    public String active(LocationChannel LC, UserData UD){
        String get = functionality.getData();
        JSONObject JSON = new JSONObject(get);
        
        String result = FunctionParser.parse(JSON, LC, UD);
        return result;
        //LC.send(UD.name, result);
    }
    
    public String attack(LocationChannel LC, UserData UD){
        
        StateMachine SM = StateMachine.SMs.get(LC.SM.getData());
        Long playerID = Long.parseLong(UD.ID);
        String state = SM.playerMap.getData().get(playerID);
        int dist = SM.distances.getData().get(state);
        
        JSONObject json = new JSONObject(combat.getData());
        if(dist==0){
            return FunctionParser.parse(json.getJSONObject("flanking"), LC, UD);
        }
        if(dist==1){
            return FunctionParser.parse(json.getJSONObject("near"), LC, UD);
        }
        if(dist==2){
            return FunctionParser.parse(json.getJSONObject("medium"), LC, UD);
        }
        if(dist==3){
            return FunctionParser.parse(json.getJSONObject("far"), LC, UD);
        }
        
        return "";
    }
    
    public static void deleteAll(){
        if(maxID == null){
            maxID = new Field("ITEM","ID",0L);
        }
        maxID.writeData(0L);
        if(defaultItems == null){
            defaultItems = new Field<>("ITEM","DEFAULT",new HashMap<>());
        }
        defaultItems.writeData(new HashMap<>());
        items = new HashMap<>();
        defaultItems.write();
    }
    
    public static void loadFromFile(){
        JSONObject json;
        File F = new File("resources/items.json");
        try{
            Scanner oScan = new Scanner(F);

            String get = "";

            while(oScan.hasNextLine()){
                get+=oScan.nextLine()+"\n";
            }

            json = new JSONObject(get);

            Iterator<?> keys = json.keys();

            
            while( keys.hasNext() ) {
                String key = (String)keys.next();
                JSONObject entry = (JSONObject)json.get(key);
                
                Item I = new Item(entry);
            }

        } catch (Exception E){E.printStackTrace();}
    }
}
