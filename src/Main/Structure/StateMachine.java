/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main.Structure;

import Bot.Fields.Field;
import Bot.Fields.UserData;
import static Bot.SuperRandom.oRan;
import Main.Channel.LocationChannel;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author FF6EB4
 */
public class StateMachine{
    
    public Field<String> name;
    public Field<Long> maxLife;
    public Field<Long> lifeForce;
    public Field<String> reward;
    
    public Field<ArrayList<String>> states;
    public Field<HashMap<String,Integer>> distances;
    public Field<HashMap<String,Integer>> stances;
    
    public static final int FLANKING = 0;
    public static final int NEAR = 1;
    public static final int MEDIUM = 2;
    public static final int FAR = 3;
    
    public static final int OFFENSE = 0;
    public static final int DEFENSE = 1;
    public static final int PARRY = 2;
    public static final int DODGE = 3;
    
    public Field<HashMap<String,String>> moves;
    
    public Field<HashMap<String,ArrayList<String>>> stateMovesPlayer;
    public Field<HashMap<String,ArrayList<String>>> stateMovesEnemy;
    
    public Field<HashMap<Long,String>>playerMap;
    
    public long ID;
    public static Field<Long> nextID;
    
    public static HashMap<Long, StateMachine> SMs = new HashMap<>();
    public static HashMap<String,Long> SMNames = loadSMs();
    
    public static HashMap<String,Long> loadSMs(){
        if(nextID == null){
            nextID = new Field<>("SM","NEXTID",0L);
        }
        HashMap<String,Long> ret = new HashMap<>();
        
        for(int i = 0; i < nextID.getData(); ++i){
            StateMachine load = new StateMachine(i);
            SMs.put(load.ID,load);
            ret.put(load.name.getData(),load.ID);
        }
        
        return ret;
    }
    
    public StateMachine(long ID){
        this.ID = ID;
        
        name = new Field<>("SM_"+ID,"NAME","Enemy");
        lifeForce = new Field<>("SM_"+ID,"LF",0L);
        maxLife = new Field<>("SM_"+ID,"MLF",0L);
        reward = new Field<>("SM_"+ID,"REWARD","");
        
        states = new Field<>("SM_"+ID,"STATES",new ArrayList<>());
        distances = new Field<>("SM_"+ID,"DIST",new HashMap<>());
        stances = new Field<>("SM_"+ID,"STANCE",new HashMap<>());
        
        moves = new Field<>("SM_"+ID,"MOVES",new HashMap<>());
        stateMovesPlayer = new Field<>("SM_"+ID,"SMP",new HashMap<>());
        stateMovesEnemy = new Field<>("SM_"+ID,"SME",new HashMap<>());
        
        playerMap = new Field<>("SM_"+ID,"PM",new HashMap<>());
    }
    
    public StateMachine(String S){
        if(nextID == null){
            nextID = new Field<>("SM","NEXTID",0L);
        }
        ID = nextID.getData();
        nextID.writeData(nextID.getData()+1);
        
        
        name = new Field<>("SM_"+ID,"NAME","Enemy");
        lifeForce = new Field<>("SM_"+ID,"LF",0L);
        maxLife = new Field<>("SM_"+ID,"MLF",0L);
        reward = new Field<>("SM_"+ID,"REWARD","");
        
        states = new Field<>("SM_"+ID,"STATES",new ArrayList<>());
        distances = new Field<>("SM_"+ID,"DIST",new HashMap<>());
        stances = new Field<>("SM_"+ID,"STANCE",new HashMap<>());
        
        moves = new Field<>("SM_"+ID,"MOVES",new HashMap<>());
        stateMovesPlayer = new Field<>("SM_"+ID,"SMP",new HashMap<>());
        stateMovesEnemy = new Field<>("SM_"+ID,"SME",new HashMap<>());
        
        playerMap = new Field<>("SM_"+ID,"PM",new HashMap<>());
        
        //Load the file
        
        JSONObject json = new JSONObject(S);
        
        name.writeData(json.getString("name"));
        lifeForce.writeData(json.getLong("life"));
        maxLife.writeData(lifeForce.getData());
        reward.writeData(json.getJSONObject("reward").toString());
        states.writeData(new ArrayList<>());
        stateMovesPlayer.writeData(new HashMap<>());
        stateMovesEnemy.writeData(new HashMap<>());
        moves.writeData(new HashMap<>());
        
        
        JSONArray arry = (JSONArray) json.get("states");
        
        for(Object O : arry){
            String state = (String)O;
            states.getData().add(state);
            
                    
            JSONObject jState = (JSONObject) json.get(state);
            JSONArray player = jState.getJSONArray("player");
            JSONArray enemy = jState.getJSONArray("enemy");
            
            distances.getData().put(state,jState.getInt("distance"));
            stances.getData().put(state,jState.getInt("stance"));
            
            distances.write();
            stances.write();
            
            stateMovesPlayer.getData().put(state, new ArrayList<>());
            for(Object O2 : player){
                stateMovesPlayer.getData().get(state).add((String)O2);
            }
            stateMovesPlayer.write();
            
            stateMovesEnemy.getData().put(state, new ArrayList<>());
            for(Object O2 : enemy){
                stateMovesEnemy.getData().get(state).add((String)O2);
            }
            stateMovesEnemy.write();
        }
        states.write();
        
        arry = (JSONArray) json.get("moves");
        for(Object O : arry){
            String move = (String) O;
            
            JSONObject get = json.getJSONObject(move);
            moves.getData().put(move,get.toString());
        }
        
        moves.write();
    }
    
    public static StateMachine get(String name){
        if(SMNames == null){
            SMNames = loadSMs();
        }
        if(SMs.get(SMNames.get(name)) != null){
            return SMs.get(SMNames.get(name));
        } else{
            return null;
        }
    }
    
    public static void deleteAll(){
        nextID.writeData(0L);
        SMs = new HashMap<>();
        SMNames = new HashMap<>();
    }
    
    public static void loadFromFile(){
        JSONObject json;
        File F = new File("resources/combat.json");
        
        try{
            Scanner oScan = new Scanner(F);

            String get = "";
            while(oScan.hasNextLine()){
                get+=oScan.nextLine()+"\n";
            }
            json = new JSONObject(get);
            Iterator<?> keys = json.keys();

            while( keys.hasNext() ) {
                String key = (String)keys.next();
                JSONObject entry = (JSONObject)json.get(key);
                
                StateMachine SM = new StateMachine(entry.toString());
                SMs.put(SM.ID, SM);
                SMNames.put(SM.name.getData(), SM.ID);
            }
        } catch (Exception EX){EX.printStackTrace();}
    }
    
    public String getActions(UserData UD){
        String state = playerMap.getData().get(Long.parseLong(UD.ID));
        ArrayList<String> options = stateMovesPlayer.getData().get(state);
        
        String ret = "**Choose wisely:**\n";
        for(String S : options){
            ret+=S+"\n";
        }
        
        Item get = Item.getItem(UD.weapon.getData());
        
        JSONObject combat = new JSONObject(get.combat.getData());
        
        int distance = distances.getData().get(state);
        int stance = stances.getData().get(state);
        
        Iterator<?> keys = combat.keys();
        
        ArrayList<String> works = new ArrayList<>();
        while( keys.hasNext() ) {
            String key = (String)keys.next();
            JSONObject entry = (JSONObject)combat.get(key);

            JSONArray dists = entry.getJSONArray("distances");
            JSONArray stanc = entry.getJSONArray("stances");
            
            boolean worked = false;
            for(Object o : dists){
                int d = (int)o;
                if(d == distance){
                    for(Object oi : stanc){
                        int sta = (int)oi;
                        if(sta == stance){
                           worked = true;
                        }
                    }
                }
            }
            
            if(worked){
                 works.add(key);
            }
        }
        if(works.size() > 0){
                for(String s : works){
                    ret = ret+s+"\n";
                }
                return ret;
            } else {
                return ret;
            }
    }
    
    public void enemyMove(LocationChannel LC, UserData UD){
        String state = playerMap.getData().get(Long.parseLong(UD.ID));
        ArrayList<String> options = stateMovesEnemy.getData().get(state);
        String move = options.get(oRan.nextInt(options.size()));
        
        String result = FunctionParser.parse(new JSONObject(moves.getData().get(move)), LC, UD);
        
        LC.send(UD.name, result);
    }
    
    public boolean playerMove(LocationChannel LC, UserData UD, String move){
        Item get = Item.getItem(UD.weapon.getData());
        String state = playerMap.getData().get(Long.parseLong(UD.ID));
        try{
            JSONObject combat = new JSONObject(get.combat.getData());

            combat = combat.getJSONObject(move.toLowerCase());
            
            int distance = distances.getData().get(state);
            int stance = stances.getData().get(state);
            JSONArray dists = combat.getJSONArray("distances");
            JSONArray stanc = combat.getJSONArray("stances");
            
            boolean worked = false;
            for(Object o : dists){
                int d = (int)o;
                if(d == distance){
                    for(Object oi : stanc){
                        int sta = (int)oi;
                        if(sta == stance){
                           worked = true;
                        }
                    }
                }
            }
            
            if(worked){
                LC.send(UD.name, FunctionParser.parse(combat, LC, UD));
                return true;
            }
            
        } catch (Exception E){}
        
        ArrayList<String> options = stateMovesPlayer.getData().get(state);
        if(options.contains(move)){
            String result = FunctionParser.parse(new JSONObject(moves.getData().get(move)), LC, UD);
            if(this.lifeForce.getData() == -1){
                this.lifeForce.writeData(this.maxLife.getData());
                LC.send(UD.name, result+"\n");
            } else {
                LC.send(UD.name, result+"\n"+getActions(UD));
            }
            return true;
        } else {
            LC.send(UD.name,getActions(UD));
            return false;
        }
    }
    
    public void registerPlayer(UserData UD){
        String state = states.getData().get(0);
        
        playerMap.getData().put(Long.parseLong(UD.ID),state);
        
        playerMap.write();
    }
}
