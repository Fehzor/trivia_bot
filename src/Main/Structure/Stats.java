/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main.Structure;

import Bot.Fields.UserData;
import static Bot.SuperRandom.oRan;

/**
 *
 * @author FF6EB4
 */
public class Stats {
    
    public static final int LIFE_FORCE = 0;
    public static final int PHYSICAL = 1;
    public static final int ENDURANCE = 2;
    public static final int EMPATHY = 3;
    public static final int LUCK = 4;
    public static final int LIBIDO = 5;
    
    
    private static int [] expTillNext = load();
    public static int [] load(){
        int[]ret = new int[1000000];
        
        for(int i = 0; i < 10; ++i){
            ret[i] = (i+1)*10;
        }
        
        for(int i = 10; i < 100; ++i){
            ret[i] = ret[9]+(i-9);
        }
        
        for(int i = 100; i < 1000; ++i){
            ret[i] = ret[99]+(i-90)/10;
        }
        
        for(int i = 1000; i < 10000; ++i){
            ret[i] = ret[999]+(i-900)/100;
        }
        
        for(int i = 10000; i < 100000; ++i){
            ret[i] = ret[9999]+(i-9000)/1000;
        }
        
        for(int i = 100000; i < 1000000; ++i){
            ret[i] = ret[99999]+(i-90000)/10000;
        }
        
        return ret;
    }
    
    
    public static int getLevel(long XP){
        int ret = 0;
        while(XP >= 0){
            XP -= expTillNext[ret];
            ret++;
        }
        return ret;
    }
    
    public static int getLevel(UserData UD, int stat){
        switch(stat){
            case LIFE_FORCE:
                return getLevel(UD.LifeForceEXP.getData());
            case PHYSICAL:
                return getLevel(UD.PhysEXP.getData());
            case ENDURANCE:
                return getLevel(UD.EndureEXP.getData());
            case EMPATHY:
                return getLevel(UD.EmpEXP.getData());
            case LUCK:
                return getLevel(UD.LuckEXP.getData());
            case LIBIDO:
                return getLevel(UD.LibidoEXP.getData());
        }
        return 1;
    }
    
    public static String getStatName(int stat){
        switch(stat){
            case LIFE_FORCE:
                return "Life Force";
            case PHYSICAL:
                return "Physical";
            case ENDURANCE:
                return "Endurance";
            case EMPATHY:
                return "Empathy";
            case LUCK:
                return "Luck";
            case LIBIDO:
                return "Libido";
        }
        return "NO SUCH STAT";
    }
    
    public static int getStatNumber(String stat){
        switch(stat){
            case "LIFE_FORCE":
                return LIFE_FORCE;
            case "PHYSICAL":
                return PHYSICAL;
            case "ENDURANCE":
                return ENDURANCE;
            case "EMPATHY":
                return EMPATHY;
            case "LUCK":
                return LUCK;
            case "LIBIDO":
                return LIBIDO;
        }
        return -1;
    }
    
    public static void giveEXPtoUD(UserData UD, long amt, int stat){
        switch(stat){
            case LIFE_FORCE:
                UD.LifeForceEXP.append(amt);
                break;
            case PHYSICAL:
                UD.PhysEXP.append(amt);
                break;
            case ENDURANCE:
                UD.EndureEXP.append(amt);
                break;
            case EMPATHY:
                UD.EmpEXP.append(amt);
                break;
            case LUCK:
                UD.LuckEXP.append(amt);
                break;
            case LIBIDO:
                UD.LibidoEXP.append(amt);
                break;
        }
    }
    
    public static int rewardEXP(int level, int required, int seconds, boolean succeeded){
        int ret = 0; //The EXP needed to level up for the user.
        
        double percentage = (double)level / (double)required;
        
        if(percentage < .35){
            ret = 3;
        } else if(percentage < .7){
            ret = 10;
        } else if(percentage < 1){
            ret = 25;
        } else if(percentage < 1.5){
            ret = 10;
        } else {
            ret = 2;
        }
        
        double multiplier = (double)seconds / (double)60;
        
        ret = (int)(ret * multiplier);
        
        if(!succeeded){
            ret = ret * 2;
        }
        
        if(ret <= 0)ret = 1;
        
        return ret;
    }
    
    public static int itemEXP(int seconds, double rarity){
        int base = 10;
        
        double time = ((double)seconds / (double)60);
        
        int answer = (int)(base * time * rarity);
        
        return answer;
    }
    
    //Used for stat checks.
    //Return true if the chance succeeds, false otherwise
    public static boolean success(int actual, int target){
        double percentage = (double) actual / (double) target;
        
        percentage = percentage * percentage;
        
        if(percentage < .2){
            return false;
        }
        
        percentage = percentage - .2;
        
        percentage = percentage * 1.25;
        
        if(oRan.nextDouble() > percentage){
            return false;
        }
        
        return true;
    }
}
