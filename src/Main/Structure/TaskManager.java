/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main.Structure;

import Main.Structure.FunctionParser;
import Main.Channel.LocationChannel;
import Bot.Fields.Field;
import Bot.Fields.UserData;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author FF6EB4
 */
public class TaskManager extends Thread{
    private static TaskManager TM = new TaskManager();
    
    
    public static final long WAIT_TIME = 1000*5; //5 sec
    
    public static Field<HashMap<Long,Long>> times;
    public static Field<HashMap<Long,Long>> starts;
    public static Field<HashMap<Long,String>> actions;
    public static Field<HashMap<Long,Long>> channels;
    public static Field<HashMap<Long,Boolean>> again;
    
    private TaskManager(){
        times = new Field<>("TASKMANAGER","TIMES",new HashMap<>());
        starts = new Field<>("TASKMANAGER","STARTS",new HashMap<>());
        actions = new Field<>("TASKMANAGER","ACTIONS",new HashMap<>());
        channels = new Field<>("TASKMANAGER","CHANNELS",new HashMap<>());
        again = new Field<>("TASKMANAGER","AGAIN",new HashMap<>());
        this.start();
    }
    
    public static void deleteAll(){
        times.writeData(new HashMap<>());
        starts.writeData(new HashMap<>());
        actions.writeData(new HashMap<>());
        channels.writeData(new HashMap<>());
        again.writeData(new HashMap<>());
    }
    
    public static void add(long ID, long chan, long time, String action, boolean a){
        times.getData().put(ID,time);
        starts.getData().put(ID,System.currentTimeMillis());
        actions.getData().put(ID,action);
        channels.getData().put(ID,chan);
        again.getData().put(ID,a);
        
        times.write();
        starts.write();
        actions.write();
        channels.write();
        again.write();
    }
    
    public void run(){
        while(true){
            try {
                Thread.sleep(WAIT_TIME);
            
        
            long current = System.currentTimeMillis();
            for(long ID : times.getData().keySet()){
                if(current - starts.getData().get(ID) > times.getData().get(ID)){
                    try{
                        UserData UD = UserData.getUD(ID);
                        
                        
                        System.out.println("ID = "+channels.getData().get(ID));
                        
                        
                        LocationChannel LC = LocationChannel.mappings.get(channels.getData().get(ID));
                        String action = actions.getData().get(ID);
                        
                        String result = FunctionParser.parse(new JSONObject(action), LC, UD);
                        if(!result.equals("")){
                            //LC.send(UD.name, result);
                        }

                        if(again.getData().get(Long.parseLong(UD.ID)) == false){
                            times.getData().remove(ID);

                            times.write();
                        } else {
                            starts.getData().put(Long.parseLong(UD.ID), System.currentTimeMillis());
                        }
                    } catch (Exception E){E.printStackTrace();}
                }
            }
            
            } catch (Exception EX) {EX.printStackTrace();}
        }
    }
    
}
